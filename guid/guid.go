package guid

import (
	"github.com/google/uuid"
	"log"
)

func GetGUID() string  {
	guid, err := uuid.NewRandom()
	if err != nil {
		log.Fatal(err)
	}
	return guid.String()
}
