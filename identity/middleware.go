package identity

import (
	"context"
	"net"
	"net/http"

	"github.com/vektah/gqlparser/gqlerror"
)

type httpContext struct {
	Writer  *http.ResponseWriter
	Request http.Request
}

var clientCtxKey = "client"
var httpCtxKey = "http_context"

func newError(message string, code int) error {
	return &gqlerror.Error{
		Message: message,
		Extensions: map[string]interface{}{
			"code": code,
		},
	}
}

func ClientMiddleware() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			var client Client
			client.IP, _, _ = net.SplitHostPort(r.RemoteAddr)

			token, err := r.Cookie("access_token")
			if err == nil {
				client.getUserData(token.Value)
			}

			ctx := context.WithValue(r.Context(), clientCtxKey, &client)

			r = r.WithContext(ctx)
			next.ServeHTTP(w, r)
		})
	}
}

func HttpSessionMiddleware() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			hCtx := httpContext{
				Writer:  &w,
				Request: *r,
			}

			ctx := context.WithValue(r.Context(), httpCtxKey, &hCtx)

			r = r.WithContext(ctx)
			next.ServeHTTP(w, r)
		})
	}
}

func GetClientFromContext(ctx context.Context) *Client {
	raw, _ := ctx.Value(clientCtxKey).(*Client)
	return raw
}

func GetHttpFromContext(ctx context.Context) *httpContext {
	raw, _ := ctx.Value(httpCtxKey).(*httpContext)
	return raw
}
