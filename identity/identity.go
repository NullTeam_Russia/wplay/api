package identity

import (
	"errors"
	"github.com/google/uuid"
	"log"
	"nullteam.info/wplay/api/db"
)

const (
	SYSTEM       = 1
	ORGANIZATION = 2
	MEMBERS      = 3
	ROLES        = 4
	REASONS      = 5
	CATEGORIES   = 6
	GROUPS       = 7
	GRADES       = 8
	ITEMS        = 9
	PURCHASES    = 10
)

type RolePermission struct {
	Organization       *string
	PermissionType     int
	Resource           *string
	PermissionToCreate bool
	PermissionToRead   bool
	PermissionToUpdate bool
	PermissionToDelete bool
}

type OrganizationPermissions struct {
	ReadableResources    map[string]bool
	UpdatableResources   map[string]bool
	DeletableResources   map[string]bool
	AreAllCreatesAllowed map[int]bool
	AreAllReadsAllowed   map[int]bool
	AreAllUpdatesAllowed map[int]bool
	AreAllDeletesAllowed map[int]bool
}

type UserData struct {
	ID                      uuid.UUID
	OrganizationPermissions map[string]*OrganizationPermissions
	AreAllCreatesAllowed    map[int]bool
	AreAllReadsAllowed      map[int]bool
	AreAllUpdatesAllowed    map[int]bool
	AreAllDeletesAllowed    map[int]bool
}

type Client struct {
	IsAuthorized bool
	IP           string
	UserData     UserData
}

func (c *Client) getUserData(token string) error {
	rows := db.Query(db.GetQuery("get_user_by_token"), token)
	defer db.CloseRows(rows)
	if !rows.Next() {
		return errors.New("there are no such user")
	}

	var id uuid.UUID
	err := rows.Scan(&id)
	if err != nil {
		log.Fatal(err)
	}

	c.IsAuthorized = true
	c.UserData = GetUserDataByID(id.String())
	c.UserData.ID = id

	return nil
}

func GetUserDataByID(userID string) UserData {
	var result UserData
	result.OrganizationPermissions = make(map[string]*OrganizationPermissions)
	result.AreAllCreatesAllowed = make(map[int]bool)
	result.AreAllReadsAllowed = make(map[int]bool)
	result.AreAllUpdatesAllowed = make(map[int]bool)
	result.AreAllDeletesAllowed = make(map[int]bool)

	rows := db.Query(db.GetQuery("get_user_permissions"), userID)

	for rows.Next() {
		var permission RolePermission

		err := rows.Scan(&permission.Organization, &permission.PermissionType, &permission.Resource,
			&permission.PermissionToCreate, &permission.PermissionToRead, &permission.PermissionToUpdate, &permission.PermissionToDelete)
		if err != nil {
			log.Fatal(err)
		}

		if permission.PermissionType == SYSTEM {
			if permission.PermissionToCreate {
				result.AreAllCreatesAllowed[ORGANIZATION] = true
				result.AreAllCreatesAllowed[MEMBERS] = true
				result.AreAllCreatesAllowed[ROLES] = true
				result.AreAllCreatesAllowed[REASONS] = true
				result.AreAllCreatesAllowed[CATEGORIES] = true
				result.AreAllCreatesAllowed[GROUPS] = true
				result.AreAllCreatesAllowed[GRADES] = true
				result.AreAllCreatesAllowed[ITEMS] = true
				result.AreAllCreatesAllowed[PURCHASES] = true
			}
			if permission.PermissionToRead {
				result.AreAllReadsAllowed[ORGANIZATION] = true
				result.AreAllReadsAllowed[MEMBERS] = true
				result.AreAllReadsAllowed[ROLES] = true
				result.AreAllReadsAllowed[REASONS] = true
				result.AreAllReadsAllowed[CATEGORIES] = true
				result.AreAllReadsAllowed[GROUPS] = true
				result.AreAllReadsAllowed[GRADES] = true
				result.AreAllReadsAllowed[ITEMS] = true
				result.AreAllReadsAllowed[PURCHASES] = true
			}
			if permission.PermissionToUpdate {
				result.AreAllUpdatesAllowed[ORGANIZATION] = true
				result.AreAllUpdatesAllowed[MEMBERS] = true
				result.AreAllUpdatesAllowed[ROLES] = true
				result.AreAllUpdatesAllowed[REASONS] = true
				result.AreAllUpdatesAllowed[CATEGORIES] = true
				result.AreAllUpdatesAllowed[GROUPS] = true
				result.AreAllUpdatesAllowed[GRADES] = true
				result.AreAllUpdatesAllowed[ITEMS] = true
				result.AreAllUpdatesAllowed[PURCHASES] = true
			}
			if permission.PermissionToDelete {
				result.AreAllDeletesAllowed[ORGANIZATION] = true
				result.AreAllDeletesAllowed[MEMBERS] = true
				result.AreAllDeletesAllowed[ROLES] = true
				result.AreAllDeletesAllowed[REASONS] = true
				result.AreAllDeletesAllowed[CATEGORIES] = true
				result.AreAllDeletesAllowed[GROUPS] = true
				result.AreAllDeletesAllowed[GRADES] = true
				result.AreAllDeletesAllowed[ITEMS] = true
				result.AreAllDeletesAllowed[PURCHASES] = true
			}
			continue
		}

		if permission.Organization == nil {
			if permission.PermissionToCreate {
				result.AreAllCreatesAllowed[permission.PermissionType] = true
			}
			if permission.PermissionToRead {
				result.AreAllReadsAllowed[permission.PermissionType] = true
			}
			if permission.PermissionToUpdate {
				result.AreAllUpdatesAllowed[permission.PermissionType] = true
			}
			if permission.PermissionToDelete {
				result.AreAllDeletesAllowed[permission.PermissionType] = true
			}
			continue
		}

		organizationPermission := result.OrganizationPermissions[*permission.Organization]

		if organizationPermission == nil {
			organizationPermission = &OrganizationPermissions{}
			organizationPermission.ReadableResources = make(map[string]bool)
			organizationPermission.UpdatableResources = make(map[string]bool)
			organizationPermission.DeletableResources = make(map[string]bool)
			organizationPermission.AreAllCreatesAllowed = make(map[int]bool)
			organizationPermission.AreAllReadsAllowed = make(map[int]bool)
			organizationPermission.AreAllUpdatesAllowed = make(map[int]bool)
			organizationPermission.AreAllDeletesAllowed = make(map[int]bool)

			organizationPermission.ReadableResources[userID] = true
			organizationPermission.UpdatableResources[userID] = true
			organizationPermission.DeletableResources[userID] = true

			result.OrganizationPermissions[*permission.Organization] = organizationPermission
		}

		if permission.Resource == nil {
			if permission.PermissionToCreate {
				organizationPermission.AreAllCreatesAllowed[permission.PermissionType] = true
			}
			if permission.PermissionToRead {
				organizationPermission.AreAllReadsAllowed[permission.PermissionType] = true
			}
			if permission.PermissionToUpdate {
				organizationPermission.AreAllUpdatesAllowed[permission.PermissionType] = true
			}
			if permission.PermissionToDelete {
				organizationPermission.AreAllDeletesAllowed[permission.PermissionType] = true
			}
			continue
		}

		if permission.PermissionToRead {
			organizationPermission.ReadableResources[*permission.Resource] = true
		}
		if permission.PermissionToUpdate {
			organizationPermission.UpdatableResources[*permission.Resource] = true
		}
		if permission.PermissionToDelete {
			organizationPermission.DeletableResources[*permission.Resource] = true
		}
	}

	db.CloseRows(rows)

	return result
}

func IsAuthorized(resourceType int, organizationID string, resourceID string, userData *UserData,
	permissionToCreate bool,
	permissionToRead bool,
	permissionToUpdate bool,
	permissionToDelete bool) bool {
	isAllowedToCreate := false
	isAllowedToRead := false
	isAllowedToUpdate := false
	isAllowedToDelete := false

	organizationPermissions := userData.OrganizationPermissions[organizationID]

	if userData.AreAllCreatesAllowed[resourceType] ||
		(organizationPermissions != nil && organizationPermissions.AreAllCreatesAllowed[resourceType]) {
		isAllowedToCreate = true
	}

	if userData.AreAllReadsAllowed[resourceType] ||
		(organizationPermissions != nil &&
			(organizationPermissions.AreAllReadsAllowed[resourceType] || organizationPermissions.ReadableResources[resourceID])) {
		isAllowedToRead = true
	}

	if userData.AreAllUpdatesAllowed[resourceType] ||
		(organizationPermissions != nil &&
			(organizationPermissions.AreAllUpdatesAllowed[resourceType] || organizationPermissions.UpdatableResources[resourceID])) {
		isAllowedToUpdate = true
	}

	if userData.AreAllDeletesAllowed[resourceType] ||
		(organizationPermissions != nil &&
			(organizationPermissions.AreAllDeletesAllowed[resourceType] || organizationPermissions.DeletableResources[resourceID])) {
		isAllowedToDelete = true
	}

	return (!permissionToCreate || isAllowedToCreate) &&
		(!permissionToRead || isAllowedToRead) &&
		(!permissionToUpdate || isAllowedToUpdate) &&
		(!permissionToDelete || isAllowedToDelete)
}
