package scheduled_tasks

import (
	"fmt"
	"time"

	"github.com/jasonlvhit/gocron"
	"nullteam.info/wplay/api/db"
)

func clearTokens() {
	_ = db.Exec("DELETE FROM sessions WHERE access_token_expiry_date < $1", time.Now())
}

func Run() {
	s := gocron.NewScheduler()
	s.Every(24).Hours().Do(clearTokens)
	s.Start()
	fmt.Println("Scheduled tasks are running.")
}
