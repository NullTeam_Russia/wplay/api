package hash

import (
	"crypto/sha512"
	"encoding/hex"
)

func GetHash(input string) string {
	bytes := sha512.Sum512([]byte(input))
	return hex.EncodeToString(bytes[:])
}
