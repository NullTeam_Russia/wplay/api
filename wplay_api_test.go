package main

import (
	"github.com/99designs/gqlgen/handler"
	"github.com/stretchr/testify/require"
	"nullteam.info/wplay/api/cmd"
	"nullteam.info/wplay/api/generated"
	"nullteam.info/wplay/api/resolver"
	"testing"

	"github.com/99designs/gqlgen/client"
)

type Error struct {
	Message    string   `json:"message"`
	Path       []string `json:"path"`
	Extensions struct {
		Code int `json:"code"`
	} `json:"extensions"`
}

func TestWPlayAPI(t *testing.T) {
	cmd.InitializeServices(true)
	c := client.New(handler.GraphQL(generated.NewExecutableSchema(generated.Config{Resolvers: &resolver.Resolver{}})))

	t.Run("Authorization Successful", func(t *testing.T) {
		var response struct {
			Authorize struct {
				IP                     string `json:"ip"`
				AccessToken            string `json:"access_token"`
				RefreshToken           string `json:"refresh_token"`
				AccessTokenExpiryDate  string `json:"access_token_expiry_date"`
				RefreshTokenExpiryDate string `json:"refresh_token_expiry_date"`
			} `json:"Authorize"`
		}
		c.MustPost(`mutation {
							  Authorize(
								  login:"admin",
								  password: "qwerty123"
							  ),
							  {
								ip,
								access_token,
								refresh_token,
								access_token_expiry_date,
								refresh_token_expiry_date
							  }
							}`, &response)

		require.NotNil(t, response.Authorize.IP)
		require.NotEqual(t, "", response.Authorize.IP)
		require.NotNil(t, response.Authorize.AccessToken)
		require.NotEqual(t, "", response.Authorize.AccessToken)
		require.NotNil(t, response.Authorize.RefreshToken)
		require.NotEqual(t, "", response.Authorize.RefreshToken)
		require.NotNil(t, response.Authorize.AccessTokenExpiryDate)
		require.NotEqual(t, "", response.Authorize.AccessTokenExpiryDate)
		require.NotNil(t, response.Authorize.RefreshTokenExpiryDate)
		require.NotEqual(t, "", response.Authorize.RefreshTokenExpiryDate)
	})

	t.Run("Authorization Fail", func(t *testing.T) {
		response := make([]Error, 1)
		response[0].Path = make([]string, 1)
		c.MustPost(`mutation {
							  Authorize(
								  login:"admin",
								  password: "qwerty1234"
							  ),
							  {
								ip,
								access_token,
								refresh_token,
								access_token_expiry_date,
								refresh_token_expiry_date
							  }
							}`, &response)
		require.Equal(t, 403, response[0].Extensions.Code)
	})
}
