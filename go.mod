module nullteam.info/wplay/api

go 1.14

require (
	github.com/99designs/gqlgen v0.11.3
	github.com/go-chi/chi v4.1.1+incompatible
	github.com/google/uuid v1.1.1
	github.com/inconshreveable/mousetrap v1.0.0
	github.com/jasonlvhit/gocron v0.0.0-20200423141508-ab84337f7963
	github.com/konsorten/go-windows-terminal-sequences v1.0.2
	github.com/lib/pq v1.2.0
	github.com/matcornic/hermes/v2 v2.1.0
	github.com/rs/cors v1.7.0
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/cobra v0.0.5
	github.com/spf13/pflag v1.0.5
	github.com/vektah/gqlparser v1.3.1
	github.com/vektah/gqlparser/v2 v2.0.1
	golang.org/x/sys v0.0.0-20200116001909-b77594299b42
)
