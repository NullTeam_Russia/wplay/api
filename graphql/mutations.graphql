input NewPageArray {
    last: UUID
    per_page: Int!
}

input NewUser {
    email: String!
    surname: String!
    name: String!
    patronymic: String!
    password: String!
}

input NewOrganization {
    name: String!
    img_path: String
    categories_enabled: Boolean!
    reasons_enabled: Boolean!
    statistics_enabled: Boolean!
    market_enabled: Boolean!
    achievements_enabled: Boolean!
    teamplay_enabled: Boolean!
    anonymization_enabled: Boolean!
}

input NewRole {
    name: String!
    display: Boolean!
}

input NewRolePermission {
    type: Int!
    resource: UUID
    self: Boolean!
    create: Boolean!
    read: Boolean!
    update: Boolean!
    delete: Boolean!
}

input NewGroup {
    name: String!
    description: String
    img_path: String
}

input NewCategory {
    name: String!
    allow_negative: Boolean!
}

input NewReason {
    text: String!
}

input NewReasonGrade {
    change: Int!
}

input NewGradeChange {
    category: UUID!
    change: Int!
}

input NewItem {
    name: String!
    description: String!
    img_path: String!
    is_special: Boolean!
    is_individual: Boolean!
}

input NewPriceGroup {
    is_conjunction: Boolean!
}

input NewPrice {
    category_id: UUID!
    price: Int!
}

input EditedUser {
    email: String
    surname: String
    name: String
    patronymic: String
    password: String
}

input EditedOrganization {
    name: String
    img_path: String
    categories_enabled: Boolean
    reasons_enabled: Boolean
    statistics_enabled: Boolean
    market_enabled: Boolean
    achievements_enabled: Boolean
    teamplay_enabled: Boolean
    anonymization_enabled: Boolean
}

input EditedRole {
    name: String
    display: Boolean
}

input EditedRolePermission {
    type: Int
    resource: UUID
    self: Boolean
    create: Boolean
    read: Boolean
    update: Boolean
    delete: Boolean
}

input EditedGroup {
    name: String
    description: String
    img_path: String
}

input EditedCategory {
    name: String
    allow_negative: Boolean
}

input EditedReason {
    text: String
}

input EditedReasonGrade {
    change: Int
}

input EditedItem {
    name: String
    description: String
    img_path: String
    is_special: Boolean
    is_individual: Boolean
}

input EditedPriceGroup {
    is_conjunction: Boolean
}

input EditedPrice {
    category_id: UUID
    price: Int
}

type Mutation {
    Authorize(login: String!, password: String!): String!
    Logout: String!

    Register(user: NewUser!): String!
    EditUser(user_id: UUID!, user: EditedUser!): String! @isAuthorized
    DeleteUser(user_id: UUID!): String! @isAuthorized

    CreateOrganization(organization: NewOrganization!): String! @isAuthorized
    EditOrganization(organization_id: UUID!, organization: EditedOrganization!): String! @isAuthorized
    DeleteOrganization(organization_id: UUID!): String! @isAuthorized

    CreateOrganizationInvite(email: String!, organization_id: UUID!): String! @isAuthorized
    AcceptOrganizationInvite(organization_id: UUID!): String! @isAuthorized
    DeleteOrganizationInvite(user_id: UUID!, organization_id: UUID!): String! @isAuthorized

    CreateRole(organization_id: UUID!, role: NewRole!): Role! @isAuthorized
    EditRole(role_id: UUID!, role: EditedRole!): String! @isAuthorized
    DeleteRole(role_id: UUID!): String! @isAuthorized
    SetRole(role_id: UUID!, user_id: UUID!, display: Boolean!): String! @isAuthorized
    UnsetRole(role_id: UUID!, user_id: UUID!): String! @isAuthorized

    CreateRolePermissions(role_id: UUID!, role_permissions: [NewRolePermission!]!): String! @isAuthorized
    EditRolePermission(role_permission_id: UUID!, role_permission: EditedRolePermission!): String! @isAuthorized
    DeleteRolePermission(role_permission_id: UUID!): String! @isAuthorized

    CreateGroup(organization_id: UUID!, group: NewGroup!): Group! @isAuthorized
    EditGroup(group_id: UUID!, group: EditedGroup!): String! @isAuthorized
    DeleteGroup(group_id: UUID!): String! @isAuthorized
    SetGroupAdmin(group_id: UUID!, user_id: UUID!): String! @isAuthorized

    CreateGroupInvite(user_id: UUID!, group_id: UUID!): String! @isAuthorized
    SetGroupInviteAcceptance(group_id: UUID!, isAccepted: Boolean!): String! @isAuthorized
    DeleteGroupInvite(user_id: UUID!, group_id: UUID!): String! @isAuthorized

    CreateCategory(organization_id: UUID!, category: NewCategory!): Category! @isAuthorized
    EditCategory(category_id: UUID!, category: EditedCategory!): String! @isAuthorized
    DeleteCategory(category_id: UUID!): String! @isAuthorized

    CreateReason(organization_id: UUID!, reason: NewReason!): Reason! @isAuthorized
    EditReason(reason_id: UUID!, reason: EditedReason!): String! @isAuthorized
    DeleteReason(reason_id: UUID!): String! @isAuthorized

    CreateReasonGrade(reason_id: UUID!, category_id: UUID!, reason_grade: NewReasonGrade!): ReasonGrade! @isAuthorized
    EditReasonGrade(reason_grade_id: UUID!, reason_grade: EditedReasonGrade!): String! @isAuthorized
    DeleteReasonGrade(reason_grade_id: UUID!): String! @isAuthorized

    CreateGradeChange(organization_id: UUID!, user_id: UUID!, text: String!, grade_changes: [NewGradeChange!]!): String! @isAuthorized
    CreateGroupGradeChange(group_id: UUID!, text: String!, grade_changes: [NewGradeChange!]!): String! @isAuthorized
    DeleteGradeChange(grade_change_group: UUID!): String! @isAuthorized

    CreateItem(organization_id: UUID!, item: NewItem!): Item! @isAuthorized
    EditItem(item_id: UUID!, item: EditedItem!): String! @isAuthorized
    DeleteItem(item_id: UUID!): String! @isAuthorized

    CreatePriceGroup(item_id: UUID!, price_group: NewPriceGroup!): PriceGroup! @isAuthorized
    EditPriceGroup(price_group_id: UUID!, price_group: EditedPriceGroup!): String! @isAuthorized
    DeletePriceGroup(price_group_id: UUID!): String! @isAuthorized

    CreatePrice(price_group_id: UUID!, price: NewPrice!): Price! @isAuthorized
    EditPrice(price_id: UUID!, price: EditedPrice!): String! @isAuthorized
    DeletePrice(price_id: UUID!): String! @isAuthorized

    CreatePurchase(price_group_id: UUID!): String! @isAuthorized
    DeletePurchase(purchase_id: UUID!): String! @isAuthorized
}