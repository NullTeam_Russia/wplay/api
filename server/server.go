package server

import (
	"log"
	"net/http"
	"os"

	"github.com/go-chi/chi"
	"github.com/rs/cors"
	"nullteam.info/wplay/api/conf"
	"nullteam.info/wplay/api/db/dataloaders"
	"nullteam.info/wplay/api/identity"

	"github.com/99designs/gqlgen/handler"
	"nullteam.info/wplay/api/generated"
	"nullteam.info/wplay/api/resolver"
)

const defaultPort = "443"

func redirectToHTTPS(w http.ResponseWriter, req *http.Request) {
	target := "https://" + req.Host + req.URL.Path
	if len(req.URL.RawQuery) > 0 {
		target += "?" + req.URL.RawQuery
	}
	http.Redirect(w, req, target,
		http.StatusTemporaryRedirect)
}

func Run() {
	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}

	router := chi.NewRouter()

	isLocalStage := conf.Configuration.Server.Stage == conf.StageLocal
	isProduction := conf.Configuration.Server.Stage == conf.StageProd

	if isLocalStage {
		router.Use(cors.New(cors.Options{
			AllowedOrigins:   []string{"http://127.0.0.1:8080", "http://localhost:8080", "http://192.168.1.10:8081"},
			AllowCredentials: true,
			Debug:            true,
		}).Handler)
	}

	if isProduction {
		router.Use(cors.New(cors.Options{
			AllowedOrigins:   []string{"https://wplay.nullteam.info"},
			AllowCredentials: true,
			Debug:            false,
		}).Handler)
	}

	config := generated.Config{Resolvers: &resolver.Resolver{}}
	config.Directives.IsAuthorized = resolver.IsAuthorizedDirective
	config.Directives.IsSelfUser = resolver.IsSelfUserDirective

	router.Use(identity.ClientMiddleware())
	router.Use(identity.HttpSessionMiddleware())
	router.Use(dataloaders.DataloadersMiddleware)

	router.Handle("/", handler.Playground("GraphQL playground", "/query"))
	router.Handle("/query", handler.GraphQL(generated.NewExecutableSchema(config)))

	if isLocalStage {
		log.Fatal(http.ListenAndServe(":8081", router))
	} else {
		go http.ListenAndServe(":80", router)

		log.Printf("connect to https://localhost:%s/ for GraphQL playground", port)
		log.Fatal(http.ListenAndServeTLS(":"+port,
			conf.Configuration.Server.TLSCertPath,
			conf.Configuration.Server.PrivKeyPath, router))
	}

}
