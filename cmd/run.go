package cmd

import (
	"github.com/spf13/cobra"
	"nullteam.info/wplay/api/conf"
	"nullteam.info/wplay/api/db"
	"nullteam.info/wplay/api/mail"
	"nullteam.info/wplay/api/scheduled_tasks"
	"nullteam.info/wplay/api/server"
)

func InitializeServices(flush bool) {
	conf.Init()
	db.InitReader()
	db.InitPostgres(&conf.Configuration.DB)
	mail.InitMail(&conf.Configuration.Mail)

	if flush {
		_ = db.Exec(db.GetProcedure("flush"))
	}

	_ = db.Exec(db.GetProcedure("add_admin"))

	scheduled_tasks.Run()
}

var RunCmd = &cobra.Command{
	Use:   "run",
	Short: "Run server",
	Run: func(cmd *cobra.Command, args []string) {
		InitializeServices(false)
		server.Run()
	},
}