package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"os"
)

var rootCmd = &cobra.Command{
	Use:   "wplay_api",
	Short: "WPlay API web server",
	Long: `WPlay API web server`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("WPlay API web server")
	},
}

func init()  {
	rootCmd.AddCommand(MigrateUpMaxCmd)
	rootCmd.AddCommand(MigrateUpCmd)
	rootCmd.AddCommand(MigrateDownCmd)
	rootCmd.AddCommand(MigrateVersionCmd)
	rootCmd.AddCommand(RunCmd)
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}