package cmd

import (
	"github.com/spf13/cobra"
	"log"
	"nullteam.info/wplay/api/conf"
	"nullteam.info/wplay/api/db"
	"strconv"
)

var MigrateUpMaxCmd = &cobra.Command{
	Use:   "migration-up-max",
	Short: "up structures to newest version",
	Run: func(cmd *cobra.Command, args []string) {
		conf.Init()
		db.InitPostgres(&conf.Configuration.DB)
		db.MigrationUpMax()
		db.Shutdown()
	},
}

var MigrateUpCmd = &cobra.Command{
	Use:   "migration-up",
	Short: "up structures",
	Run: func(cmd *cobra.Command, args []string) {
		conf.Init()
		db.InitPostgres(&conf.Configuration.DB)
		db.MigrationUp()
		db.Shutdown()
	},
}

var MigrateDownCmd = &cobra.Command{
	Use:   "migration-down",
	Short: "down structures",
	Run: func(cmd *cobra.Command, args []string) {
		conf.Init()
		db.InitPostgres(&conf.Configuration.DB)
		db.MigrationDown()
		db.Shutdown()
	},
}

var MigrateVersionCmd = &cobra.Command{
	Use:   "migration-version",
	Short: "up or down migration to the version",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		version, err := strconv.Atoi(args[0])
		if err != nil {
			log.Fatal("failed to parse version parameter, ", err)
		} else if version < 0 {
			log.Fatal("version should be greater than 0")
		}

		db.InitPostgres(&conf.Configuration.DB)
		db.Version(uint(version))
		db.Shutdown()
	},
}
