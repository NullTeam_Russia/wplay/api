package resolver

import (
	"context"
	"strconv"
	"time"

	"github.com/google/uuid"
	"nullteam.info/wplay/api/db"
	"nullteam.info/wplay/api/generated"
	"nullteam.info/wplay/api/identity"
	"nullteam.info/wplay/api/models"
)

type roleResolver struct{ *Resolver }

func (r *Resolver) Role() generated.RoleResolver {
	return &roleResolver{r}
}

func (r *roleResolver) RolePermissions(ctx context.Context, role *models.Role, pageArray models.NewPageArray) (*models.RolePermissions, error) {
	var rolePermissions models.RolePermissions
	models.RetrieveAllEntities(&rolePermissions, db.GetQuery("get_role_permission_by_role_id"),
		true, pageArray.Last, pageArray.PerPage, role.ID)
	return &rolePermissions, nil
}

// ----------------
// Role mutations
// ----------------

func (r *mutationResolver) CreateRole(ctx context.Context, organizationID models.UUID, role models.NewRole) (*models.Role, error) {
	client := identity.GetClientFromContext(ctx)

	rows := db.Query(db.GetQuery("get_organization_by_id"), organizationID)
	if !rows.Next() {
		return nil, newError("organization does not exist", 404)
	}
	db.CloseRows(rows)

	if !identity.IsAuthorized(identity.ROLES, string(organizationID), "", &client.UserData,
		true, false, false, false) {
		return nil, newError("not allowed", 403)
	}

	roleID := uuid.New()
	_ = db.Exec(db.GetProcedure("add_role"), roleID, time.Now(), role.Name, organizationID, role.Display, false)

	return &models.Role{
		ID:           models.UUID(roleID.String()),
		Name:         role.Name,
		Organization: &organizationID,
		Display:      role.Display,
	}, nil
}

func (r *mutationResolver) EditRole(ctx context.Context, roleID models.UUID, role models.EditedRole) (string, error) {
	client := identity.GetClientFromContext(ctx)

	organizationID, err := models.RetrieveOrganizationIDByRoleID(string(roleID))
	if err != nil {
		return "", err
	}

	if !identity.IsAuthorized(identity.ROLES, organizationID, string(roleID), &client.UserData,
		false, false, true, false) {
		return "", newError("not allowed", 403)
	}

	fields := make([]string, 0)
	values := make([]string, 0)

	if role.Name != nil {
		fields = append(fields, "name")
		values = append(values, "'"+*role.Name+"'")
	}
	if role.Display != nil {
		fields = append(fields, "display")
		values = append(values, strconv.FormatBool(*role.Display))
	}

	request := db.BuildRequest("UPDATE", "roles", fields, values)
	_ = db.Exec(request+" WHERE uuid=$1;", roleID)
	return "edited", nil
}

func (r *mutationResolver) DeleteRole(ctx context.Context, roleID models.UUID) (string, error) {
	client := identity.GetClientFromContext(ctx)

	organizationID, err := models.RetrieveOrganizationIDByRoleID(string(roleID))
	if err != nil {
		return "", err
	}

	if !identity.IsAuthorized(identity.ROLES, organizationID, string(roleID), &client.UserData,
		false, false, false, true) {
		return "", newError("not allowed", 403)
	}

	_ = db.Exec(db.GetProcedure("delete_role_by_id"), roleID)

	return "deleted", nil
}

func (r *mutationResolver) SetRole(ctx context.Context, roleID models.UUID, userID models.UUID, display bool) (string, error) {
	client := identity.GetClientFromContext(ctx)

	organizationID, err := models.RetrieveOrganizationIDByRoleID(string(roleID))
	if err != nil {
		return "", err
	}

	if !identity.IsAuthorized(identity.MEMBERS, organizationID, string(userID), &client.UserData,
		false, false, true, false) {
		return "", newError("not allowed", 403)
	}

	rows := db.Query(db.GetQuery("get_organization_invite_by_user_id_and_organization_id"), userID, organizationID)
	if !rows.Next() {
		return "", newError("organization does not have such user", 404)
	}
	db.CloseRows(rows)

	rows = db.Query(db.GetQuery("get_user_role_by_user_id_and_role_id"), userID, roleID)
	if rows.Next() {
		return "", newError("role already set", 406)
	}
	db.CloseRows(rows)

	_ = db.Exec(db.GetProcedure("add_user_role"), uuid.New(), time.Now(), userID, roleID, display)
	return "set", nil
}

func (r *mutationResolver) UnsetRole(ctx context.Context, roleID models.UUID, userID models.UUID) (string, error) {
	client := identity.GetClientFromContext(ctx)

	organizationID, err := models.RetrieveOrganizationIDByRoleID(string(roleID))
	if err != nil {
		return "", err
	}

	if !identity.IsAuthorized(identity.MEMBERS, organizationID, string(userID), &client.UserData,
		false, false, true, false) {
		return "", newError("not allowed", 403)
	}

	rows := db.Query(db.GetQuery("get_organization_invite_by_user_id_and_organization_id"), userID, organizationID)
	if !rows.Next() {
		return "", newError("organization does not have such user", 404)
	}
	db.CloseRows(rows)

	rows = db.Query(db.GetQuery("get_user_role_by_user_id_and_role_id"), userID, roleID)
	if !rows.Next() {
		return "", newError("role not set", 406)
	}
	db.CloseRows(rows)

	_ = db.Exec(db.GetProcedure("delete_user_role_by_user_id_and_role_id"), userID, roleID)
	return "unset", nil
}

// --------------------------
// RolePermission mutations
// --------------------------

func (r *mutationResolver) CreateRolePermissions(
	ctx context.Context,
	roleID models.UUID,
	rolePermissions []*models.NewRolePermission) (string, error) {
	client := identity.GetClientFromContext(ctx)

	rows := db.Query(db.GetQuery("get_role_by_id"), roleID)
	if !rows.Next() {
		return "", newError("role does not exist", 404)
	}
	db.CloseRows(rows)

	organizationID, err := models.RetrieveOrganizationIDByRoleID(string(roleID))
	if err != nil {
		return "", err
	}

	if !identity.IsAuthorized(identity.ROLES, organizationID, string(roleID), &client.UserData,
		true, false, false, false) {
		return "", newError("not allowed", 403)
	}

	db.BeginTransaction()

	for _, rolePermission := range rolePermissions {
		rolePermissionID := uuid.New()
		_ = db.Exec(db.GetProcedure("add_role_permission"),
			rolePermissionID,
			time.Now(),
			roleID,
			rolePermission.Type,
			rolePermission.Resource,
			rolePermission.Self,
			rolePermission.Create,
			rolePermission.Read,
			rolePermission.Update,
			rolePermission.Delete)
	}

	db.EndTransaction()

	return "created", nil
}

func (r *mutationResolver) EditRolePermission(
	ctx context.Context,
	rolePermissionID models.UUID,
	rolePermission models.EditedRolePermission) (string, error) {
	client := identity.GetClientFromContext(ctx)

	roleID, err := models.RetrieveRoleIDByRolePermissionID(string(rolePermissionID))
	if err != nil {
		return "", err
	}

	organizationID, err := models.RetrieveOrganizationIDByRoleID(roleID)
	if err != nil {
		return "", err
	}

	if !identity.IsAuthorized(identity.ROLES, organizationID, roleID, &client.UserData,
		false, false, true, false) {
		return "", newError("not allowed", 403)
	}

	fields := make([]string, 0)
	values := make([]string, 0)

	if rolePermission.Type != nil {
		fields = append(fields, "permission_type")
		values = append(values, strconv.Itoa(*rolePermission.Type))
	}
	if rolePermission.Resource != nil {
		fields = append(fields, "resource")
		values = append(values, "'"+string(*rolePermission.Resource)+"'")
	}
	if rolePermission.Self != nil {
		fields = append(fields, "self")
		values = append(values, strconv.FormatBool(*rolePermission.Self))
	}
	if rolePermission.Create != nil {
		fields = append(fields, "permission_to_create")
		values = append(values, strconv.FormatBool(*rolePermission.Create))
	}
	if rolePermission.Read != nil {
		fields = append(fields, "permission_to_read")
		values = append(values, strconv.FormatBool(*rolePermission.Read))
	}
	if rolePermission.Update != nil {
		fields = append(fields, "permission_to_update")
		values = append(values, strconv.FormatBool(*rolePermission.Update))
	}
	if rolePermission.Delete != nil {
		fields = append(fields, "permission_to_delete")
		values = append(values, strconv.FormatBool(*rolePermission.Delete))
	}

	request := db.BuildRequest("UPDATE", "role_permissions", fields, values)
	_ = db.Exec(request+" WHERE uuid=$1;", rolePermissionID)

	return "edited", nil
}

func (r *mutationResolver) DeleteRolePermission(ctx context.Context, rolePermissionID models.UUID) (string, error) {
	client := identity.GetClientFromContext(ctx)

	roleID, err := models.RetrieveRoleIDByRolePermissionID(string(rolePermissionID))
	if err != nil {
		return "", err
	}

	organizationID, err := models.RetrieveOrganizationIDByRoleID(roleID)
	if err != nil {
		return "", err
	}

	if !identity.IsAuthorized(identity.ROLES, organizationID, roleID, &client.UserData,
		false, false, false, true) {
		return "", newError("not allowed", 403)
	}

	_ = db.Exec(db.GetProcedure("delete_role_permission_by_id"), rolePermissionID)

	return "deleted", nil
}
