package resolver

import (
	"context"
	"time"

	"github.com/google/uuid"
	"nullteam.info/wplay/api/db"
	"nullteam.info/wplay/api/identity"
	"nullteam.info/wplay/api/models"
)

func (r *mutationResolver) CreateGroupInvite(ctx context.Context, userID models.UUID, groupID models.UUID) (string, error) {
	client := identity.GetClientFromContext(ctx)

	rows := db.Query(db.GetQuery("get_organization_id_by_group_id"), groupID)
	if !rows.Next() {
		return "", newError("group or organization does not exist", 404)
	}

	var organizationID string
	db.Scan(rows, &organizationID)
	db.CloseRows(rows)

	rows = db.Query(db.GetQuery("get_organization_invite_by_user_id_and_organization_id"), userID, organizationID)
	if !rows.Next() {
		return "", newError("organization does not have such user", 404)
	}
	db.CloseRows(rows)

	if !identity.IsAuthorized(identity.GROUPS, organizationID, string(groupID), &client.UserData,
		false, false, true, false) {
		return "", newError("not allowed", 403)
	}

	rows = db.Query(db.GetQuery("get_group_invite_by_user_id_and_group_id"), userID, groupID)
	if rows.Next() {
		return "", newError("invitation already exists", 406)
	}
	db.CloseRows(rows)

	_ = db.Exec(db.GetProcedure("add_group_invite"), uuid.New(), time.Now(), userID, groupID)

	if *userID.String() == client.UserData.ID.String() {
		_, err := r.SetGroupInviteAcceptance(ctx, groupID, true)
		if err != nil {
			return "", err
		}
	}

	return "invitation sent", nil
}

func (r *mutationResolver) SetGroupInviteAcceptance(ctx context.Context, groupID models.UUID, isAccepted bool) (string, error) {
	client := identity.GetClientFromContext(ctx)
	result := db.Exec(db.GetProcedure("set_group_invite_confirmation"), isAccepted, client.UserData.ID.String(), groupID)

	if db.RowsAffected(&result) == 0 {
		return "", newError("invitation does not exist", 404)
	}

	memberRoleID, err := models.RetrieveMemberRoleIDByGroupID(*groupID.String())
	if err != nil {
		return "", err
	}

	if isAccepted {
		_ = db.Exec(db.GetProcedure("add_user_role"), uuid.New(), time.Now(), client.UserData.ID, memberRoleID, false)
	} else {
		_ = db.Exec(db.GetProcedure("delete_user_role_by_user_id_and_role_id"), client.UserData.ID, memberRoleID)
	}

	return "invitation accepted", nil
}

func (r *mutationResolver) DeleteGroupInvite(ctx context.Context, userID models.UUID, groupID models.UUID) (string, error) {
	client := identity.GetClientFromContext(ctx)

	rows := db.Query(db.GetQuery("get_organization_id_by_group_id"), groupID)
	if !rows.Next() {
		return "", newError("group or organization does not exist", 404)
	}

	var organizationID string
	db.Scan(rows, &organizationID)
	db.CloseRows(rows)

	rows = db.Query(db.GetQuery("get_organization_invite_by_user_id_and_organization_id"), userID, organizationID)
	if !rows.Next() {
		return "", newError("organization does not have such user", 404)
	}
	db.CloseRows(rows)

	if !identity.IsAuthorized(identity.GROUPS, organizationID, string(groupID), &client.UserData,
		false, false, true, false) {
		return "", newError("not allowed", 403)
	}

	result := db.Exec(db.GetProcedure("delete_group_invite_by_user_id_and_group_id"), userID, groupID)
	if db.RowsAffected(&result) == 0 {
		return "", newError("group invite has not been found", 404)
	}

	memberRoleID, err := models.RetrieveMemberRoleIDByGroupID(*groupID.String())
	if err != nil {
		return "", err
	}
	_ = db.Exec(db.GetProcedure("delete_user_role_by_user_id_and_role_id"), userID, memberRoleID)

	return "invitation deleted", nil
}
