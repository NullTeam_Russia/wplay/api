package resolver

import (
	"context"
	"time"

	"github.com/google/uuid"
	"nullteam.info/wplay/api/db"
	"nullteam.info/wplay/api/db/dataloaders"
	"nullteam.info/wplay/api/generated"
	"nullteam.info/wplay/api/identity"
	"nullteam.info/wplay/api/models"
)

type gradeChangeGroupResolver struct{ *Resolver }

func (r *Resolver) GradeChangeGroup() generated.GradeChangeGroupResolver {
	return &gradeChangeGroupResolver{r}
}

func (r *gradeChangeGroupResolver) GradeChanges(ctx context.Context, gcg *models.GradeChangeGroup) ([]*models.GradeChange, error) {
	return dataloaders.GetDataloadersFromContext(ctx).GradeChangeByGradeChangeGroupID.Load(gcg.ID)
}

// -----------
// Mutations
// -----------

type gradeChangeInfo struct {
	gradeID       models.UUID
	currentValue  int
	change        int
	allowNegative bool
}

func createGradeChange(client *identity.Client, organizationID string, userID string, text string, gradeChanges []*models.NewGradeChange) error {
	userData := identity.GetUserDataByID(userID)

	db.BeginTransaction()

	gradeChangeGroupID := uuid.New()
	db.Exec(db.GetProcedure("add_grade_change_group"),
		gradeChangeGroupID, time.Now(),
		organizationID, client.UserData.ID,
		userID, text)

	allSkipped := true

	for _, gradeChange := range gradeChanges {
		if !identity.IsAuthorized(
			identity.GRADES,
			organizationID,
			string(gradeChange.Category),
			&client.UserData, false, false, true, false) {
			db.RollbackTransaction()
			return newError("not allowed", 403)
		}

		if !identity.IsAuthorized(
			identity.GRADES,
			organizationID,
			string(gradeChange.Category),
			&userData, false, true, false, false) {
			continue
		} else {
			allSkipped = false
		}

		category, err := models.RetrieveCategoryByIDandOrganizaionID(string(gradeChange.Category), organizationID)
		if err != nil {
			db.RollbackTransaction()
			return err
		}

		gradeID := uuid.New().String()
		gradeValue := 0

		grade, err := models.RetrieveGradeByUserIDandCategoryID(userID, string(gradeChange.Category))
		if err != nil {
			if !category.AllowNegative && gradeChange.Change < 0 {
				gradeValue = 0
			} else  {
				gradeValue = gradeChange.Change
			}
			db.Exec(db.GetProcedure("add_grade"), gradeID, time.Now(), userID, gradeChange.Category, gradeValue)
		} else {
			gradeID = string(grade.ID)
			gradeValue = grade.Value + gradeChange.Change
			if !category.AllowNegative && gradeValue < 0 {
				gradeValue = 0
			}
			db.Exec(db.GetProcedure("update_grade_value_by_id"), gradeValue, grade.ID)
		}

		db.Exec(db.GetProcedure("add_grade_change"), uuid.New().String(), gradeChangeGroupID, gradeChange.Category, gradeChange.Change)
	}

	if allSkipped {
		db.RollbackTransaction()
	}

	db.EndTransaction()

	return nil
}

func (r *mutationResolver) CreateGradeChange(
	ctx context.Context,
	organizationID models.UUID,
	userID models.UUID,
	text string,
	gradeChanges []*models.NewGradeChange,
) (string, error) {
	client := identity.GetClientFromContext(ctx)

	err := createGradeChange(client, string(organizationID), string(userID), text, gradeChanges)
	if err != nil {
		return "", err
	}

	return "changed", nil
}

func (r *mutationResolver) CreateGroupGradeChange(ctx context.Context, groupID models.UUID, text string, gradeChanges []*models.NewGradeChange) (string, error) {
	client := identity.GetClientFromContext(ctx)

	organizationID, err := models.RetrieveOrganizationIDByGroupID(*groupID.String())
	if err != nil {
		return "", err
	}

	if !identity.IsAuthorized(identity.GROUPS, organizationID, "", &client.UserData,
		false, true, false, false) {
		return "", newError("group is not available", 403)
	}

	rows := db.Query(db.GetQuery("get_user_id_by_group_id"), groupID)

	for rows.Next() {
		var userID string
		err := rows.Scan(&userID)
		if err != nil {
			return "", err
		}

		err = createGradeChange(client, organizationID, userID, text, gradeChanges)
		if err != nil {
			return "", nil
		}
	}

	return "created", nil
}

func deleteGradeChangeByGradeChangeGroup(gradeChangeGroup *models.GradeChangeGroup) error {
	db.BeginTransaction()
	rows := db.Query(db.GetQuery("get_grade_change_info_by_grade_change_group_id_and_user_id"), gradeChangeGroup.ID, gradeChangeGroup.User)
	changesInfo := make([]gradeChangeInfo, 0)
	for rows.Next() {
		var changeInfo gradeChangeInfo
		db.Scan(rows, &changeInfo.gradeID, &changeInfo.currentValue, &changeInfo.change, &changeInfo.allowNegative)
		changeInfo.currentValue -= changeInfo.change
		if !changeInfo.allowNegative && changeInfo.currentValue < 0 {
			changeInfo.currentValue = 0
		}
		changesInfo = append(changesInfo, changeInfo)
	}
	db.CloseRows(rows)

	for _, changeInfo := range changesInfo {
		db.Exec(db.GetProcedure("update_grade_value_by_id"), changeInfo.currentValue, changeInfo.gradeID)
	}

	db.Exec(db.GetProcedure("delete_grade_change_group"), gradeChangeGroup.ID)
	db.EndTransaction()

	return nil
}

func deleteGradeChangeByID(gradeChangeGroupID string) error {
	var gradeChangeGroup models.GradeChangeGroup
	if !models.RetrieveEntity(&gradeChangeGroup, db.GetQuery("get_grade_change_group_by_id"), gradeChangeGroupID) {
		return newError("grade change group does not exist", 404)
	}
	return deleteGradeChangeByGradeChangeGroup(&gradeChangeGroup)
}

func (r *mutationResolver) DeleteGradeChange(ctx context.Context, gradeChangeGroupID models.UUID) (string, error) {
	client := identity.GetClientFromContext(ctx)

	gradeChangeGroup, err := models.RetrieveGradeChangeGroupByID(string(gradeChangeGroupID))
	if err != nil {
		return "", err
	}

	if string(gradeChangeGroup.Issuer) != client.UserData.ID.String() && !identity.IsAuthorized(identity.GRADES,
		string(gradeChangeGroup.Organization),
		uuid.Nil.String(),
		&client.UserData,
		false, false, false, true) {
		return "", newError("not allowed", 403)
	}

	err = deleteGradeChangeByGradeChangeGroup(&gradeChangeGroup)
	if err != nil {
		return "", err
	}

	return "deleted", err
}
