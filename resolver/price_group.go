package resolver

import (
	"context"
	"github.com/google/uuid"
	"nullteam.info/wplay/api/db"
	"nullteam.info/wplay/api/generated"
	"nullteam.info/wplay/api/identity"
	"nullteam.info/wplay/api/models"
	"strconv"
	"time"
)

type priceGroupResolver struct{ *Resolver }

func (r * Resolver) PriceGroup() generated.PriceGroupResolver  {
	return &priceGroupResolver{r}
}

func (p priceGroupResolver) Prices(ctx context.Context, priceGroup *models.PriceGroup) ([]*models.Price, error) {
	var prices models.Prices
	models.RetrieveAllEntities(&prices, db.GetQuery("get_prices_by_price_group_id"),
		false, nil, 0, priceGroup.ID)

	return prices.Data, nil
}

func (r *mutationResolver) CreatePriceGroup(ctx context.Context, itemID models.UUID, priceGroup models.NewPriceGroup) (*models.PriceGroup, error) {
	client := identity.GetClientFromContext(ctx)

	organizationID, err := models.RetrieveOrganizationIDByItemID(*itemID.String())
	if err != nil {
		return nil, newError("item or organization does not exist", 404)
	}

	if !identity.IsAuthorized(identity.ITEMS, organizationID, "", &client.UserData,
		false, false, true, false) {
		return nil, newError("not allowed", 403)
	}

	priceGroupID := uuid.New()

	_ = db.Exec(db.GetProcedure("add_price_group"),
		priceGroupID,
		time.Now(),
		itemID,
		priceGroup.IsConjunction)

	return &models.PriceGroup{
		ID:            models.UUID(priceGroupID.String()),
		ItemID:        itemID,
		IsConjunction: priceGroup.IsConjunction,
	}, nil
}

func (r *mutationResolver) EditPriceGroup(ctx context.Context, priceGroupID models.UUID, priceGroup models.EditedPriceGroup) (string, error) {
	client := identity.GetClientFromContext(ctx)

	itemID, err := models.RetrieveItemIDByPriceGroupID(*priceGroupID.String())
	if err != nil {
		return "", newError("price group or item does not exist", 404)
	}

	organizationID, err := models.RetrieveOrganizationIDByItemID(itemID)
	if err != nil {
		return "", newError("item or organization does not exist", 404)
	}

	if !identity.IsAuthorized(identity.ITEMS, organizationID, itemID, &client.UserData,
		false, false, true, false) {
		return "", newError("not allowed", 403)
	}

	fields := make([]string, 0)
	values := make([]string, 0)

	if priceGroup.IsConjunction != nil {
		fields = append(fields, "is_conjunction")
		values = append(values, strconv.FormatBool(*priceGroup.IsConjunction))
	}

	request := db.BuildRequest("UPDATE", "price_groups", fields, values)
	_ = db.Exec(request+" WHERE uuid=$1;", priceGroupID)

	return "edited", nil
}

func (r *mutationResolver) DeletePriceGroup(ctx context.Context, priceGroupID models.UUID) (string, error) {
	client := identity.GetClientFromContext(ctx)

	itemID, err := models.RetrieveItemIDByPriceGroupID(*priceGroupID.String())
	if err != nil {
		return "", newError("price group or item does not exist", 404)
	}

	organizationID, err := models.RetrieveOrganizationIDByItemID(itemID)
	if err != nil {
		return "", newError("item or organization does not exist", 404)
	}

	if !identity.IsAuthorized(identity.ITEMS, organizationID, itemID, &client.UserData,
		false, false, true, false) {
		return "", newError("not allowed", 403)
	}

	_ = db.Exec(db.GetProcedure("delete_price_group_by_id"), priceGroupID)

	return "deleted", nil
}
