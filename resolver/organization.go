package resolver

import (
	"context"
	"log"
	"strconv"
	"time"

	"github.com/google/uuid"
	"nullteam.info/wplay/api/db"
	"nullteam.info/wplay/api/generated"
	"nullteam.info/wplay/api/guid"
	"nullteam.info/wplay/api/identity"
	"nullteam.info/wplay/api/models"
)

type organizationResolver struct{ *Resolver }

func (r *Resolver) Organization() generated.OrganizationResolver {
	return &organizationResolver{r}
}

func (r *organizationResolver) Members(ctx context.Context, organization *models.Organization, pageArray models.NewPageArray) (*models.Users, error) {
	client := identity.GetClientFromContext(ctx)

	var members models.Users
	models.RetrieveEntities(&members, db.GetQuery("get_user_by_organization_id"), identity.MEMBERS,
		*organization.ID.String(), &client.UserData, true, pageArray.Last, pageArray.PerPage, organization.ID)

	return &members, nil
}

func (r *organizationResolver) Roles(ctx context.Context, organization *models.Organization, pageArray models.NewPageArray) (*models.Roles, error) {
	client := identity.GetClientFromContext(ctx)

	var roles models.Roles
	models.RetrieveEntities(&roles, db.GetQuery("get_roles_by_organization_id"), identity.ROLES,
		*organization.ID.String(), &client.UserData, true, pageArray.Last, pageArray.PerPage, organization.ID)

	return &roles, nil
}

func (r *organizationResolver) Reasons(ctx context.Context, organization *models.Organization, pageArray models.NewPageArray) (*models.Reasons, error) {
	client := identity.GetClientFromContext(ctx)

	var reasons models.Reasons
	models.RetrieveEntities(&reasons, db.GetQuery("get_reasons_by_organization_id"), identity.REASONS,
		*organization.ID.String(), &client.UserData, true, pageArray.Last, pageArray.PerPage, organization.ID)

	return &reasons, nil
}

func (r *organizationResolver) Categories(ctx context.Context, organization *models.Organization, pageArray models.NewPageArray) (*models.Categories, error) {
	client := identity.GetClientFromContext(ctx)

	var categories models.Categories
	if organization.CategoriesEnabled {
		models.RetrieveEntities(&categories, db.GetQuery("get_categories_by_organization_id"), identity.CATEGORIES,
			*organization.ID.String(), &client.UserData, true, pageArray.Last, pageArray.PerPage, organization.ID, false)
	} else {
		models.RetrieveEntities(&categories, db.GetQuery("get_categories_by_organization_id"), identity.CATEGORIES,
			*organization.ID.String(), &client.UserData, true, pageArray.Last, pageArray.PerPage, organization.ID, true)
	}

	return &categories, nil
}

func (r *organizationResolver) Groups(ctx context.Context, organization *models.Organization, pageArray models.NewPageArray) (*models.Groups, error) {
	client := identity.GetClientFromContext(ctx)

	var groups models.Groups
	models.RetrieveEntities(&groups, db.GetQuery("get_groups_by_organization_id"), identity.GROUPS,
		*organization.ID.String(), &client.UserData, true, pageArray.Last, pageArray.PerPage, organization.ID)

	return &groups, nil
}

func (r *organizationResolver) Items(ctx context.Context, organization *models.Organization, pageArray models.NewPageArray) (*models.Items, error) {
	client := identity.GetClientFromContext(ctx)

	if !organization.MarketEnabled {
		return &models.Items{}, nil
	}

	var items models.Items
	models.RetrieveEntities(&items, db.GetQuery("get_items_by_organization_id"), identity.ITEMS,
		*organization.ID.String(), &client.UserData, true, pageArray.Last, pageArray.PerPage, organization.ID)

	return &items, nil
}

func (r *organizationResolver) Purchases(ctx context.Context, organization *models.Organization, pageArray models.NewPageArray) (*models.Purchases, error) {
	client := identity.GetClientFromContext(ctx)

	if !organization.MarketEnabled {
		return &models.Purchases{}, nil
	}

	var purchases models.Purchases
	models.RetrieveEntities(&purchases, db.GetQuery("get_purchases_by_organization_id"), identity.PURCHASES,
		*organization.ID.String(), &client.UserData, true, pageArray.Last, pageArray.PerPage, organization.ID)

	return &purchases, nil
}

// -----------
// Mutations
// -----------

func (r *mutationResolver) CreateOrganization(ctx context.Context, organization models.NewOrganization) (string, error) {
	client := identity.GetClientFromContext(ctx)

	if !identity.IsAuthorized(identity.ORGANIZATION, "", "", &client.UserData,
		true, false, false, false) {
		return "", newError("not allowed", 403)
	}

	id := guid.GetGUID()
	_ = db.Exec(db.GetProcedure("add_organization"), id, time.Now(), organization.Name,
		organization.CategoriesEnabled, organization.ReasonsEnabled, organization.StatisticsEnabled, organization.MarketEnabled,
		organization.AchievementsEnabled, organization.TeamplayEnabled, organization.AnonymizationEnabled)

	createdOrganization := models.Organization{
		ID:                   models.UUID(id),
		Name:                 organization.Name,
		ImgPath:              nil,
		CategoriesEnabled:    organization.CategoriesEnabled,
		ReasonsEnabled:       organization.ReasonsEnabled,
		StatisticsEnabled:    organization.StatisticsEnabled,
		MarketEnabled:        organization.MarketEnabled,
		AchievementsEnabled:  organization.AchievementsEnabled,
		TeamplayEnabled:      organization.TeamplayEnabled,
		AnonymizationEnabled: organization.AnonymizationEnabled,
	}

	_, err := CreateBaseCategory(createdOrganization.ID)
	if err != nil {
		return "", newError("base category creation error", 201)
	}

	return "created", nil

}

func (r *mutationResolver) EditOrganization(ctx context.Context, organizationID models.UUID, organization models.EditedOrganization) (string, error) {
	client := identity.GetClientFromContext(ctx)

	if !identity.IsAuthorized(identity.ORGANIZATION, string(organizationID), string(organizationID), &client.UserData,
		false, false, true, false) {
		return "", newError("not allowed", 403)
	}

	fields := make([]string, 0)
	values := make([]string, 0)

	if organization.Name != nil {
		fields = append(fields, "name")
		values = append(values, "'"+*organization.Name+"'")
	}
	if organization.ImgPath != nil {
		fields = append(fields, "img_path")
		values = append(values, "'"+*organization.ImgPath+"'")
	}
	if organization.CategoriesEnabled != nil {
		fields = append(fields, "categories")
		values = append(values, strconv.FormatBool(*organization.CategoriesEnabled))
	}
	if organization.ReasonsEnabled != nil {
		fields = append(fields, "reasons")
		values = append(values, strconv.FormatBool(*organization.ReasonsEnabled))
	}
	if organization.StatisticsEnabled != nil {
		fields = append(fields, "statistics")
		values = append(values, strconv.FormatBool(*organization.StatisticsEnabled))
	}
	if organization.MarketEnabled != nil {
		fields = append(fields, "market")
		values = append(values, strconv.FormatBool(*organization.MarketEnabled))
	}
	if organization.AchievementsEnabled != nil {
		fields = append(fields, "achievements")
		values = append(values, strconv.FormatBool(*organization.AchievementsEnabled))
	}
	if organization.TeamplayEnabled != nil {
		fields = append(fields, "teamplay")
		values = append(values, strconv.FormatBool(*organization.TeamplayEnabled))
	}
	if organization.AnonymizationEnabled != nil {
		fields = append(fields, "anonymization")
		values = append(values, strconv.FormatBool(*organization.AnonymizationEnabled))
	}

	request := db.BuildRequest("UPDATE", "organizations", fields, values)
	result := db.Exec(request+" WHERE uuid=$1;", organizationID)
	rowsAffected, err := result.RowsAffected()
	if err != nil {
		log.Fatal(err)
	}
	if rowsAffected == 0 {
		return "organization does not exist", nil
	}

	return "edited", nil
}

func (r *mutationResolver) DeleteOrganization(ctx context.Context, organizationID models.UUID) (string, error) {
	client := identity.GetClientFromContext(ctx)

	if !identity.IsAuthorized(identity.ORGANIZATION, string(organizationID), string(organizationID), &client.UserData,
		false, false, false, true) {
		return "", newError("not allowed", 403)
	}

	_ = db.Exec(db.GetProcedure("delete_organization_by_id"), organizationID)
	return "deleted", nil
}

// ---------
// Invites
// ---------

func (r *mutationResolver) CreateOrganizationInvite(ctx context.Context, email string, organizationID models.UUID) (string, error) {
	client := identity.GetClientFromContext(ctx)

	if !identity.IsAuthorized(identity.MEMBERS, string(organizationID), "", &client.UserData,
		true, false, false, false) {
		return "", newError("not allowed", 403)
	}

	rows := db.Query(db.GetQuery("get_user_id_by_email"), email)
	var userID string
	if !rows.Next() {
		return "", newError("user does not exist", 404)
	}
	db.Scan(rows, &userID)
	db.CloseRows(rows)

	rows = db.Query(db.GetQuery("get_organization_invite_by_user_id_and_organization_id"), userID, organizationID)

	if rows.Next() {
		return "", newError("invitation already exists", 406)
	}

	db.CloseRows(rows)

	_ = db.Exec(db.GetProcedure("add_organization_invite"), uuid.New(), time.Now(), userID, organizationID)

	return "invitation sent", nil
}

func (r *mutationResolver) AcceptOrganizationInvite(ctx context.Context, organizationID models.UUID) (string, error) {
	client := identity.GetClientFromContext(ctx)
	result := db.Exec(db.GetProcedure("set_organization_invite_confirmation"), true, client.UserData.ID.String(), organizationID)

	if db.RowsAffected(&result) == 0 {
		return "", newError("invitation does not exist", 404)
	}
	return "invitation accepted", nil
}

func (r *mutationResolver) DeleteOrganizationInvite(ctx context.Context, userID models.UUID, organizationID models.UUID) (string, error) {
	client := identity.GetClientFromContext(ctx)

	rows := db.Query(db.GetQuery("get_organization_invite_by_user_id_and_organization_id"), userID, organizationID)
	if !rows.Next() {
		return "", newError("organization does not have such user", 404)
	}
	db.CloseRows(rows)

	if !identity.IsAuthorized(identity.MEMBERS, string(organizationID), string(userID), &client.UserData,
		false, false, false, true) {
		return "", newError("not allowed", 403)
	}

	result := db.Exec(db.GetProcedure("delete_organization_invite_by_user_id_and_organization_id"), userID, organizationID)
	if db.RowsAffected(&result) == 0 {
		return "", newError("organization invite has not been found", 404)
	}

	return "invitation deleted", nil
}
