package resolver

import (
	"context"
	"github.com/google/uuid"
	"nullteam.info/wplay/api/db"
	"nullteam.info/wplay/api/identity"
	"nullteam.info/wplay/api/models"
	"strconv"
	"time"
)

func (r *mutationResolver) CreatePrice(ctx context.Context, priceGroupID models.UUID, price models.NewPrice) (*models.Price, error) {
	client := identity.GetClientFromContext(ctx)

	itemID, err := models.RetrieveItemIDByPriceGroupID(*priceGroupID.String())
	if err != nil {
		return nil, newError("group or item does not exist", 404)
	}

	organizationID, err := models.RetrieveOrganizationIDByItemID(itemID)
	if err != nil {
		return nil, newError("item or organization does not exist", 404)
	}

	if !identity.IsAuthorized(identity.ITEMS, organizationID, "", &client.UserData,
		false, false, true, false) {
		return nil, newError("not allowed", 403)
	}

	priceID := uuid.New()

	_ = db.Exec(db.GetProcedure("add_price"),
		priceID,
		time.Now(),
		priceGroupID,
		price.CategoryID,
		price.Price)

	return &models.Price{
		ID:           models.UUID(priceID.String()),
		PriceGroupID: priceGroupID,
		CategoryID:   price.CategoryID,
		Price:        price.Price,
	}, nil
}

func (r *mutationResolver) EditPrice(ctx context.Context, priceID models.UUID, price models.EditedPrice) (string, error) {
	client := identity.GetClientFromContext(ctx)

	priceGroupID, err := models.RetrievePriceGroupIDByPriceID(*priceID.String())
	if err != nil {
		return "", newError("price or price group does not exist", 404)
	}

	itemID, err := models.RetrieveItemIDByPriceGroupID(priceGroupID)
	if err != nil {
		return "", newError("price group or item does not exist", 404)
	}

	organizationID, err := models.RetrieveOrganizationIDByItemID(itemID)
	if err != nil {
		return "", newError("item or organization does not exist", 404)
	}

	if !identity.IsAuthorized(identity.ITEMS, organizationID, itemID, &client.UserData,
		false, false, true, false) {
		return "", newError("not allowed", 403)
	}

	fields := make([]string, 0)
	values := make([]string, 0)

	if price.Price != nil {
		fields = append(fields, "price")
		values = append(values, strconv.Itoa(*price.Price))
	}
	if price.CategoryID != nil {
		_, err := models.RetrieveCategoryByIDandOrganizaionID(*price.CategoryID.String(), organizationID)
		if err != nil {
			return "", newError("category does not exist", 404)
		}

		fields = append(fields, "category_id")
		values = append(values, "'"+*price.CategoryID.String()+"'")
	}

	request := db.BuildRequest("UPDATE", "prices", fields, values)
	_ = db.Exec(request+" WHERE uuid=$1;", priceID)

	return "edited", nil
}

func (r *mutationResolver) DeletePrice(ctx context.Context, priceID models.UUID) (string, error) {
	client := identity.GetClientFromContext(ctx)

	priceGroupID, err := models.RetrievePriceGroupIDByPriceID(*priceID.String())
	if err != nil {
		return "", newError("price or price group does not exist", 404)
	}

	itemID, err := models.RetrieveItemIDByPriceGroupID(priceGroupID)
	if err != nil {
		return "", newError("price group or item does not exist", 404)
	}

	organizationID, err := models.RetrieveOrganizationIDByItemID(itemID)
	if err != nil {
		return "", newError("item or organization does not exist", 404)
	}

	if !identity.IsAuthorized(identity.ITEMS, organizationID, itemID, &client.UserData,
		false, false, true, false) {
		return "", newError("not allowed", 403)
	}

	_ = db.Exec(db.GetProcedure("delete_price_by_id"), priceID)

	return "deleted", nil
}
