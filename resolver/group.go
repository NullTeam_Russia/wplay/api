package resolver

import (
	"context"
	"time"

	"nullteam.info/wplay/api/generated"

	"github.com/google/uuid"
	"nullteam.info/wplay/api/db"
	"nullteam.info/wplay/api/identity"
	"nullteam.info/wplay/api/models"
)

type groupResolver struct{ *Resolver }

func (r *Resolver) Group() generated.GroupResolver {
	return &groupResolver{r}
}

func (g *groupResolver) Members(ctx context.Context, group *models.Group, pageArray models.NewPageArray) (*models.Users, error) {
	client := identity.GetClientFromContext(ctx)

	var members models.Users
	models.RetrieveEntities(&members, db.GetQuery("get_user_by_group_id"), identity.MEMBERS,
		*group.Organization.String(), &client.UserData, true, pageArray.Last, pageArray.PerPage, group.ID)

	return &members, nil
}

func (r *mutationResolver) CreateGroup(ctx context.Context, organizationID models.UUID, group models.NewGroup) (*models.Group, error) {
	client := identity.GetClientFromContext(ctx)

	if !identity.IsAuthorized(identity.GROUPS, string(organizationID), "", &client.UserData,
		true, false, false, false) {
		return nil, newError("not allowed", 403)
	}

	rows := db.Query(db.GetQuery("get_organization_by_id"), organizationID)
	if !rows.Next() {
		return nil, newError("organization not found", 404)
	}
	db.CloseRows(rows)

	groupID := uuid.New()

	_ = db.Exec(db.GetProcedure("add_group"),
		groupID,
		time.Now(),
		group.Name,
		organizationID,
		group.Description,
		group.ImgPath)

	// GROUP ADMINISTRATOR ROLE CREATION

	roleID := uuid.New()
	_ = db.Exec(db.GetProcedure("add_role"), roleID, time.Now(), "Group administrator", organizationID, false, true)

	rolePermissionID := uuid.New()
	_ = db.Exec(db.GetProcedure("add_role_permission"),
		rolePermissionID,
		time.Now(),
		roleID,
		identity.GROUPS,
		groupID,
		false,
		true,
		true,
		true,
		true)

	_ = db.Exec(db.GetProcedure("add_user_role"), uuid.New(), time.Now(), client.UserData.ID, roleID, false)

	// GROUP MEMBER ROLE CREATION

	roleID = uuid.New()
	_ = db.Exec(db.GetProcedure("add_role"), roleID, time.Now(), "Group member", organizationID, false, true)

	rolePermissionID = uuid.New()
	_ = db.Exec(db.GetProcedure("add_role_permission"),
		rolePermissionID,
		time.Now(),
		roleID,
		identity.GROUPS,
		groupID,
		false,
		false,
		true,
		false,
		false)

	return &models.Group{
		ID:           models.UUID(groupID.String()),
		Organization: organizationID,
		Name:         group.Name,
		Description:  group.Description,
		ImgPath:      group.ImgPath,
	}, nil
}

func (r *mutationResolver) EditGroup(ctx context.Context, groupID models.UUID, group models.EditedGroup) (string, error) {
	client := identity.GetClientFromContext(ctx)

	rows := db.Query(db.GetQuery("get_organization_id_by_group_id"), groupID)
	if !rows.Next() {
		return "", newError("group or organization does not exist", 404)
	}

	var organizationID string
	db.Scan(rows, &organizationID)
	db.CloseRows(rows)

	if !identity.IsAuthorized(identity.GROUPS, organizationID, string(groupID), &client.UserData,
		false, false, true, false) {
		return "", newError("not allowed", 403)
	}

	fields := make([]string, 0)
	values := make([]string, 0)

	if group.Name != nil {
		fields = append(fields, "name")
		values = append(values, "'"+*group.Name+"'")
	}
	if group.Description != nil {
		fields = append(fields, "description")
		values = append(values, "'"+*group.Description+"'")
	}
	if group.ImgPath != nil {
		fields = append(fields, "img_path")
		values = append(values, "'"+*group.ImgPath+"'")
	}

	request := db.BuildRequest("UPDATE", "groups", fields, values)
	_ = db.Exec(request+" WHERE uuid=$1;", groupID)

	return "edited", nil
}

func (r *mutationResolver) DeleteGroup(ctx context.Context, groupID models.UUID) (string, error) {
	client := identity.GetClientFromContext(ctx)

	rows := db.Query(db.GetQuery("get_organization_id_by_group_id"), groupID)
	if !rows.Next() {
		return "", newError("group or organization does not exist", 404)
	}

	var organizationID string
	db.Scan(rows, &organizationID)
	db.CloseRows(rows)

	if !identity.IsAuthorized(identity.GROUPS, organizationID, string(groupID), &client.UserData,
		false, false, false, true) {
		return "", newError("not allowed", 403)
	}

	_ = db.Exec(db.GetProcedure("delete_group_by_id"), groupID)
	_ = db.Exec(db.GetProcedure("delete_technical_role_by_resource"), groupID)

	return "deleted", nil
}

func (r *mutationResolver) SetGroupAdmin(ctx context.Context, groupID models.UUID, userID models.UUID) (string, error) {
	client := identity.GetClientFromContext(ctx)

	organizationID, err := models.RetrieveOrganizationIDByGroupID(*groupID.String())
	if err != nil {
		return "", err
	}

	adminRoleID, err := models.RetrieveAdministratorRoleIDByGroupID(*groupID.String())
	if err != nil {
		return "", err
	}

	adminUserID, err := models.RetrieveUserIDByRoleID(adminRoleID)
	if err != nil {
		return "", err
	}

	if adminUserID != client.UserData.ID.String() &&
		!identity.IsAuthorized(identity.ORGANIZATION, organizationID, "", &client.UserData,
			false, false, true, false) {
		return "", newError("user is not an administrator", 403)
	}

	_ = db.Exec(db.GetProcedure("update_user_id_by_role_id"), userID, adminRoleID)
	return "set", nil
}
