package resolver

import (
	"context"
	"strconv"
	"time"

	"github.com/google/uuid"
	"nullteam.info/wplay/api/db"
	"nullteam.info/wplay/api/identity"
	"nullteam.info/wplay/api/models"
)

func addCategory(organizationID models.UUID, category models.Category) (*models.Category, error) {
	rows := db.Query(db.GetQuery("get_organization_by_id"), organizationID)
	if !rows.Next() {
		return nil, newError("organization not found", 404)
	}
	db.CloseRows(rows)

	categoryID := uuid.New()

	_ = db.Exec(db.GetProcedure("add_category"),
		categoryID,
		time.Now(),
		category.Name,
		category.Base,
		organizationID,
		category.AllowNegative)

	return &models.Category{
		ID:            models.UUID(categoryID.String()),
		Organization:  organizationID,
		Name:          category.Name,
		Base:          category.Base,
		AllowNegative: category.AllowNegative,
	}, nil
}

func getCategory(categoryID models.UUID) (*models.Category, error) {
	var category models.Category

	rows := db.Query(db.GetQuery("get_category_by_id"), categoryID)
	defer db.CloseRows(rows)

	if rows.Next() {
		err := category.Scan(rows)
		if err != nil {
			return nil, err
		}
	}
	return &category, nil
}

func CreateBaseCategory(organizationID models.UUID) (*models.Category, error) {
	category := models.Category{
		Name:          "Базовая категория",
		Base:          true,
		AllowNegative: false,
	}
	return addCategory(organizationID, category)
}

func (r *mutationResolver) CreateCategory(
	ctx context.Context,
	organizationID models.UUID,
	newCategory models.NewCategory,
) (*models.Category, error) {
	client := identity.GetClientFromContext(ctx)

	if !identity.IsAuthorized(identity.CATEGORIES, string(organizationID), "", &client.UserData,
		true, false, false, false) {
		return nil, newError("not allowed", 403)
	}

	category := models.Category{
		Name:          newCategory.Name,
		Base:          false,
		AllowNegative: newCategory.AllowNegative,
	}

	return addCategory(organizationID, category)
}

func (r *mutationResolver) EditCategory(ctx context.Context, categoryID models.UUID, category models.EditedCategory) (string, error) {
	client := identity.GetClientFromContext(ctx)

	organizationID, err := models.RetrieveOrganizationIDByCategoryID(string(categoryID))
	if err != nil {
		return "", err
	}

	if !identity.IsAuthorized(identity.CATEGORIES, organizationID, string(categoryID), &client.UserData,
		false, false, true, false) {
		return "", newError("not allowed", 403)
	}

	categoryCurrentState, err := getCategory(categoryID)
	if err != nil {
		return "", err
	}

	fields := make([]string, 0)
	values := make([]string, 0)

	if category.Name != nil && !categoryCurrentState.Base {
		fields = append(fields, "name")
		values = append(values, "'"+*category.Name+"'")
	}
	if category.AllowNegative != nil {
		fields = append(fields, "allow_negative")
		values = append(values, strconv.FormatBool(*category.AllowNegative))
	}

	request := db.BuildRequest("UPDATE", "categories", fields, values)
	_ = db.Exec(request+" WHERE uuid=$1;", categoryID)

	return "edited", nil
}

func (r *mutationResolver) DeleteCategory(ctx context.Context, categoryID models.UUID) (string, error) {
	client := identity.GetClientFromContext(ctx)

	rows := db.Query(db.GetQuery("get_organization_id_by_category_id"), categoryID)
	if !rows.Next() {
		return "", newError("category or organization does not exist", 404)
	}

	var organizationID string
	db.Scan(rows, &organizationID)

	// Is category base?
	category, err := getCategory(categoryID)
	if err != nil {
		return "", err
	}
	if category.Base {
		return "", newError("cannot delete base category", 403)
	}

	if !identity.IsAuthorized(identity.CATEGORIES, organizationID, string(categoryID), &client.UserData,
		false, false, false, true) {
		return "", newError("not allowed", 403)
	}

	_ = db.Exec(db.GetProcedure("delete_category_by_id"), categoryID)

	return "deleted", nil
}
