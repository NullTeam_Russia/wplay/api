package resolver

import (
	"context"
	"strconv"
	"time"

	"github.com/google/uuid"
	"nullteam.info/wplay/api/db"
	"nullteam.info/wplay/api/db/dataloaders"
	"nullteam.info/wplay/api/generated"
	"nullteam.info/wplay/api/identity"
	"nullteam.info/wplay/api/models"
)

type reasonResolver struct{ *Resolver }

func (r *Resolver) Reason() generated.ReasonResolver {
	return &reasonResolver{r}
}

func (r *reasonResolver) ReasonGrades(ctx context.Context, reason *models.Reason) ([]*models.ReasonGrade, error) {
	return dataloaders.GetDataloadersFromContext(ctx).ReasonGradeByReasonID.Load(reason.ID)
}

func (r *mutationResolver) CreateReason(ctx context.Context, organizationID models.UUID, reason models.NewReason) (*models.Reason, error) {
	client := identity.GetClientFromContext(ctx)

	if !identity.IsAuthorized(identity.REASONS, string(organizationID), "", &client.UserData,
		true, false, false, false) {
		return nil, newError("not allowed", 403)
	}

	rows := db.Query(db.GetQuery("get_organization_by_id"), organizationID)
	if !rows.Next() {
		return nil, newError("organization not found", 404)
	}
	db.CloseRows(rows)

	reasonID := uuid.New()

	_ = db.Exec(db.GetProcedure("add_reason"),
		reasonID,
		time.Now(),
		reason.Text,
		organizationID)

	return &models.Reason{
		ID:           models.UUID(reasonID.String()),
		Organization: organizationID,
		Text:         reason.Text,
	}, nil
}

func (r *mutationResolver) EditReason(ctx context.Context, reasonID models.UUID, reason models.EditedReason) (string, error) {
	client := identity.GetClientFromContext(ctx)

	rows := db.Query(db.GetQuery("get_organization_id_by_reason_id"), reasonID)
	if !rows.Next() {
		return "", newError("reason or organization does not exist", 404)
	}

	var organizationID string
	db.Scan(rows, &organizationID)
	db.CloseRows(rows)

	if !identity.IsAuthorized(identity.REASONS, organizationID, string(reasonID), &client.UserData,
		false, false, true, false) {
		return "", newError("not allowed", 403)
	}

	fields := make([]string, 0)
	values := make([]string, 0)

	if reason.Text != nil {
		fields = append(fields, "text")
		values = append(values, "'"+*reason.Text+"'")
	}

	request := db.BuildRequest("UPDATE", "reasons", fields, values)
	_ = db.Exec(request+" WHERE uuid=$1;", reasonID)

	return "edited", nil
}

func (r *mutationResolver) DeleteReason(ctx context.Context, reasonID models.UUID) (string, error) {
	client := identity.GetClientFromContext(ctx)

	rows := db.Query(db.GetQuery("get_organization_id_by_reason_id"), reasonID)
	if !rows.Next() {
		return "", newError("reason or organization does not exist", 404)
	}

	var organizationID string
	db.Scan(rows, &organizationID)
	db.CloseRows(rows)

	if !identity.IsAuthorized(identity.REASONS, organizationID, string(reasonID), &client.UserData,
		false, false, false, true) {
		return "", newError("not allowed", 403)
	}

	_ = db.Exec(db.GetProcedure("delete_reason_by_id"), reasonID)

	return "deleted", nil
}

// ---------------
// Reason Grades
// ---------------

func (r *mutationResolver) CreateReasonGrade(
	ctx context.Context,
	reasonID models.UUID,
	categoryID models.UUID,
	reasonGrade models.NewReasonGrade) (*models.ReasonGrade, error) {
	client := identity.GetClientFromContext(ctx)

	rows := db.Query(db.GetQuery("get_reason_by_id"), reasonID)
	if !rows.Next() {
		return nil, newError("reason does not exist", 404)
	}
	db.CloseRows(rows)

	organizationID, err := models.RetrieveOrganizationIDByReasonID(string(reasonID))
	if err != nil {
		return nil, err
	}

	if !identity.IsAuthorized(identity.REASONS, organizationID, string(reasonID), &client.UserData,
		true, false, false, false) {
		return nil, newError("not allowed", 403)
	}

	reasonGradeID := uuid.New()
	_ = db.Exec(db.GetProcedure("add_reason_grade"),
		reasonGradeID,
		time.Now(),
		reasonID,
		categoryID,
		reasonGrade.Change)

	return &models.ReasonGrade{
		ID:       models.UUID(reasonGradeID.String()),
		Reason:   reasonID,
		Category: categoryID,
		Change:   reasonGrade.Change,
	}, nil
}

func (r *mutationResolver) EditReasonGrade(ctx context.Context, reasonGradeID models.UUID, reasonGrade models.EditedReasonGrade) (string, error) {
	client := identity.GetClientFromContext(ctx)

	reasonID, err := models.RetrieveReasonIDByReasonGradeID(string(reasonGradeID))
	if err != nil {
		return "", err
	}

	organizationID, err := models.RetrieveOrganizationIDByReasonID(reasonID)
	if err != nil {
		return "", err
	}

	if !identity.IsAuthorized(identity.REASONS, organizationID, reasonID, &client.UserData,
		false, false, true, false) {
		return "", newError("not allowed", 403)
	}

	fields := make([]string, 0)
	values := make([]string, 0)

	if reasonGrade.Change != nil {
		fields = append(fields, "change")
		values = append(values, strconv.Itoa(*reasonGrade.Change))
	}

	request := db.BuildRequest("UPDATE", "reason_grades", fields, values)
	_ = db.Exec(request+" WHERE uuid=$1;", reasonGradeID)

	return "edited", nil
}

func (r *mutationResolver) DeleteReasonGrade(ctx context.Context, reasonGradeID models.UUID) (string, error) {
	client := identity.GetClientFromContext(ctx)

	reasonID, err := models.RetrieveReasonIDByReasonGradeID(string(reasonGradeID))
	if err != nil {
		return "", err
	}

	organizationID, err := models.RetrieveOrganizationIDByReasonID(reasonID)
	if err != nil {
		return "", err
	}

	if !identity.IsAuthorized(identity.REASONS, organizationID, reasonID, &client.UserData,
		false, false, false, true) {
		return "", newError("not allowed", 403)
	}

	_ = db.Exec(db.GetProcedure("delete_reason_grade_by_id"), reasonGradeID)

	return "deleted", nil
}
