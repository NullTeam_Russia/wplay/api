package resolver

import (
	"context"
	"net/http"
	"time"

	"nullteam.info/wplay/api/conf"
	"nullteam.info/wplay/api/db"
	"nullteam.info/wplay/api/guid"
	"nullteam.info/wplay/api/hash"
	"nullteam.info/wplay/api/identity"
)

func (r *mutationResolver) Authorize(ctx context.Context, login string, password string) (string, error) {
	client := identity.GetClientFromContext(ctx)

	rows := db.Query("SELECT uuid FROM users WHERE email = $1 AND password = $2;", login, hash.GetHash(password))
	if !rows.Next() {
		return "", newError("wrong login or password", 403)
	}
	var userID string
	db.Scan(rows, &userID)
	db.CloseRows(rows)

	sessionID := guid.GetGUID()
	accessToken := guid.GetGUID()
	accessTokenExpiryDate := time.Now().AddDate(0, 3, 0)

	_ = db.Exec("INSERT INTO sessions (uuid, created_at, user_id, ip, access_token, access_token_expiry_date) VALUES ($1, $2, $3, $4, $5, $6);",
		sessionID, time.Now(), userID, client.IP, accessToken, accessTokenExpiryDate)

	httpContext := identity.GetHttpFromContext(ctx)
	http.SetCookie(*httpContext.Writer, &http.Cookie{
		Name:     "access_token",
		Value:    accessToken,
		Expires:  accessTokenExpiryDate,
		HttpOnly: true,
		Secure:   conf.Configuration.Server.Stage != conf.StageLocal,
		Path:     "/",
	})

	return "authorized", nil
}

func (r *mutationResolver) Logout(ctx context.Context) (string, error) {
	httpContext := identity.GetHttpFromContext(ctx)
	http.SetCookie(*httpContext.Writer, &http.Cookie{
		Name:   "access_token",
		Value:  "",
		MaxAge: -1,
	})

	return "logout", nil
}
