package resolver

import (
	"context"
	"github.com/google/uuid"
	"nullteam.info/wplay/api/db"
	"nullteam.info/wplay/api/generated"
	"nullteam.info/wplay/api/identity"
	"nullteam.info/wplay/api/models"
	"time"
)

type purchaseResolver struct{ *Resolver }

func (r *Resolver) Purchase() generated.PurchaseResolver {
	return &purchaseResolver{r}
}

func (r *purchaseResolver) Item(ctx context.Context, purchase *models.Purchase) (*models.Item, error) {
	var item models.Item
	if !models.RetrieveEntity(&item, db.GetQuery("get_item_by_id"), purchase.ItemID) {
		return nil, newError("item does not exist", 404)
	}
	return &item, nil
}

func (r *mutationResolver) CreatePurchase(ctx context.Context, priceGroupID models.UUID) (string, error) {
	client := identity.GetClientFromContext(ctx)

	itemID, err := models.RetrieveItemIDByPriceGroupID(*priceGroupID.String())
	if err != nil {
		return "", newError("price group or item does not exist", 404)
	}

	organizationID, err := models.RetrieveOrganizationIDByItemID(itemID)
	if err != nil {
		return "", newError("organization or item does not exist", 404)
	}

	var organization models.Organization
	if !models.RetrieveEntity(&organization, db.GetQuery("get_organization_by_id"), organizationID) {
		return "", newError("organization does not exist", 404)
	}

	if !organization.MarketEnabled {
		return "", newError("market is not enabled", 403)
	}

	if !identity.IsAuthorized(identity.PURCHASES, organizationID, itemID, &client.UserData,
		true, false, false, false) {
		return "", newError("not allowed", 403)
	}

	var prices models.Prices
	models.RetrieveAllEntities(&prices, db.GetQuery("get_prices_by_price_group_id"),
		false, nil, 0, priceGroupID)

	db.BeginTransaction()

	gradeChangeGroupID := uuid.New()
	db.Exec(db.GetProcedure("add_grade_change_group"),
		gradeChangeGroupID, time.Now(),
		organizationID, client.UserData.ID,
		client.UserData.ID, "Purchase")

	for _, price := range prices.Data {
		_, err := models.RetrieveCategoryByIDandOrganizaionID(string(price.CategoryID), organizationID)
		if err != nil {
			db.RollbackTransaction()
			return "", err
		}

		gradeValue := 0

		grade, err := models.RetrieveGradeByUserIDandCategoryID(client.UserData.ID.String(), string(price.CategoryID))
		if err != nil {
			db.RollbackTransaction()
			return "", newError("not enough grade", 403)
		} else {
			gradeValue = grade.Value - price.Price
			if gradeValue < 0 {
				db.RollbackTransaction()
				return "", newError("not enough grade", 403)
			}
			db.Exec(db.GetProcedure("update_grade_value_by_id"), gradeValue, grade.ID)
		}

		db.Exec(db.GetProcedure("add_grade_change"), uuid.New().String(), gradeChangeGroupID, price.CategoryID, -price.Price)
	}

	purchaseID := uuid.New()

	_ = db.Exec(db.GetProcedure("add_purchase"),
		purchaseID,
		time.Now(),
		client.UserData.ID,
		itemID,
		gradeChangeGroupID)

	db.EndTransaction()

	return "created", nil
}

func (r *mutationResolver) DeletePurchase(ctx context.Context, purchaseID models.UUID) (string, error) {
	client := identity.GetClientFromContext(ctx)

	var purchase models.Purchase
	if !models.RetrieveEntity(&purchase, db.GetQuery("get_purchase_by_id"), purchaseID) {
		return "", newError("purchase does not exist", 404)
	}

	organizationID, err := models.RetrieveOrganizationIDByItemID(string(purchase.ItemID))
	if err != nil {
		return "", newError("item or organization does not exist", 404)
	}

	if !identity.IsAuthorized(identity.PURCHASES, organizationID, string(purchase.ItemID), &client.UserData,
		false, false, false, true) {
		return "", newError("not allowed", 403)
	}

	err = deleteGradeChangeByID(string(purchase.GradeChangeGroupID))
	if err != nil {
		return "", err
	}

	_ = db.Exec(db.GetProcedure("delete_purchase_by_id"), purchaseID)

	return "deleted", nil
}