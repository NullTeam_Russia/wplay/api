package resolver

//go:generate go run github.com/99designs/gqlgen

import (
	"context"
	"nullteam.info/wplay/api/db"

	"github.com/vektah/gqlparser/gqlerror"
	"nullteam.info/wplay/api/generated"
	"nullteam.info/wplay/api/identity"
	"nullteam.info/wplay/api/models"
)

func newError(message string, code int) error {
	return &gqlerror.Error{
		Message: message,
		Extensions: map[string]interface{}{
			"code": code,
		},
	}
}

type Resolver struct{}

func (r *Resolver) Mutation() generated.MutationResolver {
	return &mutationResolver{r}
}

func (r *Resolver) Query() generated.QueryResolver {
	return &queryResolver{r}
}

type mutationResolver struct{ *Resolver }

type queryResolver struct{ *Resolver }

func (q *queryResolver) Organization(ctx context.Context, id models.UUID) (*models.Organization, error) {
	client := identity.GetClientFromContext(ctx)

	if !identity.IsAuthorized(identity.ORGANIZATION, *id.String(), *id.String(), &client.UserData,
		false, true, false, false) {
		return nil, newError("not allowed", 403)
	}

	var result models.Organization
	if !models.RetrieveEntity(&result, db.GetQuery("get_organization_by_id"), id) {
		return nil, newError("organization does not exist", 404)
	}

	return &result, nil
}

func (q *queryResolver) Organizations(ctx context.Context, pageArray models.NewPageArray) (*models.Organizations, error) {
	client := identity.GetClientFromContext(ctx)

	if !identity.IsAuthorized(identity.ORGANIZATION, "", "", &client.UserData,
		false, true, false, false) {
		return nil, newError("not allowed", 403)
	}

	var result models.Organizations
	models.RetrieveAllEntities(&result, db.GetQuery("get_all_organizations"), true, pageArray.Last, pageArray.PerPage)

	return &result, nil
}
