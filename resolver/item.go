package resolver

import (
	"context"
	"github.com/google/uuid"
	"nullteam.info/wplay/api/db"
	"nullteam.info/wplay/api/generated"
	"nullteam.info/wplay/api/identity"
	"nullteam.info/wplay/api/models"
	"strconv"
	"time"
)

type itemResolver struct{ *Resolver }

func (r *Resolver) Item() generated.ItemResolver {
	return &itemResolver{r}
}

func (i itemResolver) Purchases(ctx context.Context, item *models.Item, pageArray models.NewPageArray) (*models.Purchases, error) {
	client := identity.GetClientFromContext(ctx)

	var purchases models.Purchases
	models.RetrieveEntities(&purchases, db.GetQuery("get_purchases_by_item_id"), identity.PURCHASES,
		*item.Organization.String(), &client.UserData, true, pageArray.Last, pageArray.PerPage, item.ID)

	return &purchases, nil
}

func (i itemResolver) PriceGroups(ctx context.Context, item *models.Item) ([]*models.PriceGroup, error) {
	var priceGroups models.PriceGroups
	models.RetrieveAllEntities(&priceGroups, db.GetQuery("get_price_groups_by_item_id"),
		false, nil, 0, item.ID)
	return priceGroups.Data, nil
}

func (r *mutationResolver) CreateItem(ctx context.Context, organizationID models.UUID, item models.NewItem) (*models.Item, error) {
	client := identity.GetClientFromContext(ctx)

	if !identity.IsAuthorized(identity.ITEMS, string(organizationID), "", &client.UserData,
		true, false, false, false) {
		return nil, newError("not allowed", 403)
	}

	rows := db.Query(db.GetQuery("get_organization_by_id"), organizationID)
	if !rows.Next() {
		return nil, newError("organization not found", 404)
	}
	db.CloseRows(rows)

	itemID := uuid.New()

	_ = db.Exec(db.GetProcedure("add_item"),
		itemID,
		time.Now(),
		item.Name,
		organizationID,
		item.Description,
		item.ImgPath,
		item.IsSpecial,
		item.IsIndividual)

	return &models.Item{
		ID:           models.UUID(itemID.String()),
		Organization: organizationID,
		Name:         item.Name,
		Description:  item.Description,
		ImgPath:      item.ImgPath,
		IsSpecial:    item.IsSpecial,
		IsIndividual: item.IsIndividual,
	}, nil
}

func (r *mutationResolver) EditItem(ctx context.Context, itemID models.UUID, item models.EditedItem) (string, error) {
	client := identity.GetClientFromContext(ctx)

	organizationID, err := models.RetrieveOrganizationIDByItemID(*itemID.String())
	if err != nil {
		return "", newError("item or organization does not exist", 404)
	}

	if !identity.IsAuthorized(identity.ITEMS, organizationID, string(itemID), &client.UserData,
		false, false, true, false) {
		return "", newError("not allowed", 403)
	}

	fields := make([]string, 0)
	values := make([]string, 0)

	if item.Name != nil {
		fields = append(fields, "name")
		values = append(values, "'"+*item.Name+"'")
	}
	if item.Description != nil {
		fields = append(fields, "description")
		values = append(values, "'"+*item.Description+"'")
	}
	if item.ImgPath != nil {
		fields = append(fields, "img_path")
		values = append(values, "'"+*item.ImgPath+"'")
	}
	if item.IsSpecial != nil {
		fields = append(fields, "is_special")
		values = append(values, strconv.FormatBool(*item.IsSpecial))
	}
	if item.IsIndividual != nil {
		fields = append(fields, "is_individual")
		values = append(values, strconv.FormatBool(*item.IsIndividual))
	}

	request := db.BuildRequest("UPDATE", "items", fields, values)
	_ = db.Exec(request+" WHERE uuid=$1;", itemID)

	return "edited", nil
}

func (r *mutationResolver) DeleteItem(ctx context.Context, itemID models.UUID) (string, error) {
	client := identity.GetClientFromContext(ctx)

	organizationID, err := models.RetrieveOrganizationIDByItemID(*itemID.String())
	if err != nil {
		return "", newError("item or organization does not exist", 404)
	}

	var organization models.Organization
	models.RetrieveEntity(&organization, db.GetQuery("get_organization_by_id"), organizationID)

	if !identity.IsAuthorized(identity.ITEMS, organizationID, string(itemID), &client.UserData,
		false, false, false, true) {
		return "", newError("not allowed", 403)
	}

	_ = db.Exec(db.GetProcedure("delete_item_by_id"), itemID)

	return "deleted", nil
}
