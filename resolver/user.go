package resolver

import (
	"context"
	"github.com/google/uuid"
	"time"

	"nullteam.info/wplay/api/db"
	"nullteam.info/wplay/api/generated"
	"nullteam.info/wplay/api/guid"
	"nullteam.info/wplay/api/hash"
	"nullteam.info/wplay/api/identity"
	"nullteam.info/wplay/api/mail"
	"nullteam.info/wplay/api/models"
)

type userResolver struct{ *Resolver }

func (r *Resolver) User() generated.UserResolver {
	return &userResolver{r}
}

func (q *queryResolver) User(ctx context.Context, userID *models.UUID) (*models.User, error) {
	client := identity.GetClientFromContext(ctx)

	var id string

	if userID == nil {
		id = client.UserData.ID.String()
	} else {
		id = string(*userID)
	}

	var result models.User
	if !models.RetrieveEntity(&result, db.GetQuery("get_user_by_id"), id) {
		return nil, newError("user does not exist", 404)
	}

	return &result, nil
}

func (u *userResolver) Member(ctx context.Context, user *models.User, organizationID models.UUID) (*models.Member, error) {
	client := identity.GetClientFromContext(ctx)

	if !identity.IsAuthorized(identity.MEMBERS, string(organizationID), string(user.ID), &client.UserData,
		false, true, false, false) {
		return nil, newError("not allowed", 403)
	}

	var result models.Member
	if !models.RetrieveEntity(&result, db.GetQuery("get_member_by_organization_id_and_user_id"), organizationID, string(user.ID)) {
		return nil, newError("member does not exist", 404)
	}

	return &result, nil
}

func (u *userResolver) Roles(ctx context.Context, user *models.User, pageArray models.NewPageArray) (*models.Roles, error) {
	var roles models.Roles
	models.RetrieveAllEntities(&roles, db.GetQuery("get_role_by_user_id"), true, pageArray.Last, pageArray.PerPage, user.ID)
	return &roles, nil
}

func (u *userResolver) Organizations(ctx context.Context, user *models.User, pageArray models.NewPageArray) (*models.Organizations, error) {
	var organizations models.Organizations
	models.RetrieveAllEntities(&organizations, db.GetQuery("get_organizations_by_user_id"),
		true, pageArray.Last, pageArray.PerPage, user.ID)
	return &organizations, nil
}

func (u *userResolver) InvitedOrganizations(ctx context.Context, user *models.User, pageArray models.NewPageArray) (*models.Organizations, error) {
	var organizations models.Organizations
	models.RetrieveAllEntities(&organizations, db.GetQuery("get_invited_organization_by_user_id"),
		true, pageArray.Last, pageArray.PerPage, user.ID)
	return &organizations, nil
}

type memberResolver struct{ *Resolver }

func (r *Resolver) Member() generated.MemberResolver {
	return &memberResolver{r}
}

func (r *memberResolver) Roles(ctx context.Context, member *models.Member, pageArray models.NewPageArray) (*models.Roles, error) {
	client := identity.GetClientFromContext(ctx)

	var roles models.Roles
	models.RetrieveEntities(&roles, db.GetQuery("get_role_by_user_id_and_organization_id"), identity.ROLES,
		string(member.OrganizationID), &client.UserData, true, pageArray.Last, pageArray.PerPage,
		member.UserID, member.OrganizationID)

	return &roles, nil
}

func (r *memberResolver) Grades(ctx context.Context, member *models.Member, pageArray models.NewPageArray) (*models.Grades, error) {
	client := identity.GetClientFromContext(ctx)

	var grades models.Grades
	models.RetrieveEntities(&grades, db.GetQuery("get_grade_by_user_id_and_organization_id"), identity.GRADES,
		string(member.OrganizationID), &client.UserData,true, pageArray.Last, pageArray.PerPage,
		member.UserID, member.OrganizationID)

	return &grades, nil
}

func (u *memberResolver) IssuedGradeChanges(
	ctx context.Context,
	member *models.Member,
	pageArray models.NewPageArray) (*models.GradeChangeGroups, error) {
	client := identity.GetClientFromContext(ctx)

	var gradeChangeGroups models.GradeChangeGroups
	models.RetrieveEntities(&gradeChangeGroups, db.GetQuery("get_grade_change_group_by_issuer_id_and_organization_id"), identity.ROLES,
		string(member.OrganizationID), &client.UserData, true, pageArray.Last, pageArray.PerPage, member.UserID, member.OrganizationID)

	return &gradeChangeGroups, nil
}

func (r *memberResolver) GradableCategories(ctx context.Context, member *models.Member, pageArray models.NewPageArray) (*models.Categories, error) {
	client := identity.GetClientFromContext(ctx)

	organizationID := member.OrganizationID.String()

	var organization models.Organization
	if !models.RetrieveEntity(&organization, db.GetQuery("get_organization_by_id"), organizationID) {
		return nil, newError("organization does not exist", 404)
	}

	memberUserData := identity.GetUserDataByID(*member.UserID.String())

	var categories models.Categories
	if organization.CategoriesEnabled {
		models.RetrieveEntities(&categories, db.GetQuery("get_categories_by_organization_id"), identity.CATEGORIES,
			string(organization.ID), &client.UserData, true, pageArray.Last, pageArray.PerPage, organization.ID, false)
	} else {
		models.RetrieveEntities(&categories, db.GetQuery("get_categories_by_organization_id"), identity.CATEGORIES,
			string(organization.ID), &client.UserData, true, pageArray.Last, pageArray.PerPage, organization.ID, true)
	}

	filteredCategories := make([]*models.Category, 0)
	for _, c := range categories.Data {
		if identity.IsAuthorized(identity.GRADES, *organizationID, *c.ID.String(), &client.UserData,
			false, false, true, false) &&
			identity.IsAuthorized(identity.GRADES, *organizationID, *c.ID.String(), &memberUserData,
				false, true, false, false){
			filteredCategories = append(filteredCategories, c)
		}
	}

	last := models.UUID(uuid.Nil.String())

	if len(filteredCategories) > 0 {
		last = filteredCategories[len(filteredCategories) - 1].ID
	}

	return &models.Categories{
		Last:		last,
		Data:       filteredCategories,
	}, nil
}

func (r *memberResolver) Purchases(ctx context.Context, member *models.Member, pageArray models.NewPageArray) (*models.Purchases, error) {
	client := identity.GetClientFromContext(ctx)

	var organization models.Organization
	if !models.RetrieveEntity(&organization, db.GetQuery("get_organization_by_id"), member.OrganizationID) {
		return nil, newError("organization does not exist", 404)
	}

	if !organization.MarketEnabled {
		return &models.Purchases{}, nil
	}

	var purchases models.Purchases
	models.RetrieveEntities(&purchases, db.GetQuery("get_purchases_by_buyer_and_organization_id"), identity.PURCHASES,
		string(organization.ID), &client.UserData, true, pageArray.Last, pageArray.PerPage,
		member.UserID, organization.ID)

	return &purchases, nil
}

// -----------
// Mutations
// -----------

func (r *mutationResolver) Register(ctx context.Context, user models.NewUser) (string, error) {
	rows := db.Query("SELECT uuid, verified, verification_email_send_date FROM users WHERE email = $1;", user.Email)
	if rows.Next() {
		var id string
		var verified bool
		var verificationEmailSendDate time.Time

		db.Scan(rows, &id, &verified, &verificationEmailSendDate)

		if verified {
			return "", newError("user already exists", 409)
		}

		if time.Now().Sub(verificationEmailSendDate).Hours() < 24 {
			return "", newError("invitation email already sent", 406)
		} else {
			verificationLink := guid.GetGUID()

			_ = db.Exec("UPDATE users SET"+
				" (password, name, patronymic, surname, verification_link, verification_email_send_date) ="+
				" ($1, $2, $3, $4, $5, $6) WHERE email = $7;",
				hash.GetHash(user.Password), user.Name, user.Patronymic, user.Surname, verificationLink, time.Now(), user.Email)

			mail.SendConfirmationEmail(user.Email, user.Name, verificationLink)

			return "", nil
		}
	}
	db.CloseRows(rows)

	id := guid.GetGUID()
	verificationLink := guid.GetGUID()
	_ = db.Exec("INSERT INTO "+
		"users (uuid, created_at, email, password, name, patronymic, surname, verified, verification_link, verification_email_send_date) "+
		"VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10);",
		id, time.Now(), user.Email, hash.GetHash(user.Password), user.Name, user.Patronymic, user.Surname, true, verificationLink, time.Now())

	//_ = db.Exec("INSERT INTO "+
	//	"users (uuid, created_at, email, password, name, patronymic, surname, verification_link, verification_email_send_date) "+
	//	"VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9);",
	//	id, time.Now(), user.Email, hash.GetHash(user.Password), user.Name, user.Patronymic, user.Surname, verificationLink, time.Now())
	//
	//mail.SendConfirmationEmail(user.Email, user.Name, verificationLink)

	return "registered", nil
}

func (r *mutationResolver) EditUser(ctx context.Context, userID models.UUID, user models.EditedUser) (string, error) {
	client := identity.GetClientFromContext(ctx)

	if client.UserData.ID.String() != *userID.String() && !identity.IsAuthorized(identity.SYSTEM, "", "",
		&client.UserData, false, false, true, false) {
		return "", newError("not allowed", 403)
	}

	fields := make([]string, 0)
	values := make([]string, 0)

	if user.Name != nil {
		fields = append(fields, "name")
		values = append(values, "'"+*user.Name+"'")
	}
	if user.Email != nil {
		fields = append(fields, "email")
		values = append(values, "'"+*user.Email+"'")
	}
	if user.Surname != nil {
		fields = append(fields, "surname")
		values = append(values, "'"+*user.Surname+"'")
	}
	if user.Patronymic != nil {
		fields = append(fields, "patronymic")
		values = append(values, "'"+*user.Patronymic+"'")
	}
	if user.Password != nil {
		fields = append(fields, "password")
		values = append(values, "'"+hash.GetHash(*user.Password)+"'")
	}

	request := db.BuildRequest("UPDATE", "users", fields, values)
	result := db.Exec(request+" WHERE uuid=$1;", userID)
	if db.RowsAffected(&result) == 0 {
		return "user does not exist", nil
	}

	return "edited", nil
}

func (r *mutationResolver) DeleteUser(ctx context.Context, userID models.UUID) (string, error) {
	client := identity.GetClientFromContext(ctx)

	if client.UserData.ID.String() != *userID.String() && !identity.IsAuthorized(identity.SYSTEM, "", "",
		&client.UserData, false, false, false, true) {
		return "", newError("not allowed", 403)
	}

	_ = db.Exec(db.GetProcedure("delete_user_by_id"), userID)
	return "deleted", nil
}
