package resolver

import (
	"context"

	"github.com/99designs/gqlgen/graphql"
	"nullteam.info/wplay/api/identity"
	"nullteam.info/wplay/api/models"
)

func IsAuthorizedDirective(ctx context.Context, obj interface{}, next graphql.Resolver) (res interface{}, err error) {
	client := identity.GetClientFromContext(ctx)
	if !client.IsAuthorized {
		return nil, newError("authorization required", 401)
	}
	return next(ctx)
}

func IsSelfUserDirective(ctx context.Context, obj interface{}, next graphql.Resolver) (res interface{}, err error) {
	client := identity.GetClientFromContext(ctx)

	requestedUser := obj.(*models.User)
	if string(requestedUser.ID) != client.UserData.ID.String() {
		return nil, nil
	}

	return next(ctx)
}
