package models

import (
	"database/sql"
	"errors"
	"fmt"
	"log"
	"nullteam.info/wplay/api/db"
	"nullteam.info/wplay/api/identity"
	"strings"
)

type ModelEntity interface {
	Scan(rows *sql.Rows) error
	GetID() UUID
}

type ModelEntities interface {
	Init()
	NewEntity() ModelEntity
	Append(entity *ModelEntity)
	SetLast(last UUID)
}

func getDBRowsByPage(query string, paged bool, page *UUID, args ...interface{}) (*sql.Rows) {
	queryFrom := query[strings.Index(query, "from"):]

	offset := 0

	if page != nil {
		row := db.Database.QueryRow(
			fmt.Sprintf("select row_number from (select uuid, row_number () over (order by created_at) %s) a where a.uuid = '%s';",
				queryFrom[:len(queryFrom)-1], string(*page)),
			args...)

		err := row.Scan(&offset)
		if err != nil {
			log.Fatal(err)
		}
	}

	var rows *sql.Rows

	if paged {
		rows = db.Query(query[:len(query)-1]+fmt.Sprintf(" order by created_at offset %d;", offset), args...)
	} else {
		rows = db.Query(query, args...)
	}

	return rows
}

func RetrieveAllEntities(entities ModelEntities, query string, paged bool, last *UUID, perPage int, args ...interface{}) {
	entities.Init()
	rows := getDBRowsByPage(query, paged, last, args...)
	page := 0

	var entity ModelEntity

	for rows.Next() && page < perPage {
		entity = entities.NewEntity()
		err := entity.Scan(rows)
		if err != nil {
			log.Fatal(err)
		}
		entities.Append(&entity)
		page++
	}
	if entity != nil {
		entities.SetLast(entity.GetID())
	}
}

func RetrieveEntities(entities ModelEntities, query string, resourceType int, organizationID string, userData *identity.UserData, paged bool, last *UUID, perPage int, args ...interface{}) {
	entities.Init()
	rows := getDBRowsByPage(query, paged, last, args...)
	page := 0

	var entity ModelEntity

	for rows.Next() && page < perPage {
		entity = entities.NewEntity()
		err := entity.Scan(rows)
		if err != nil {
			log.Fatal(err)
		}
		if !identity.IsAuthorized(resourceType, organizationID, string(entity.GetID()), userData,
			false, true, false, false) {
			continue
		}
		entities.Append(&entity)
		page++
	}
	if entity != nil {
		entities.SetLast(entity.GetID())
	}
}

func RetrieveEntity(entity ModelEntity, query string, args ...interface{}) bool {
	rows, err := db.Database.Query(query, args...)
	if err != nil {
		return false
	}
	if !rows.Next() {
		return false
	}
	err = entity.Scan(rows)
	if err != nil {
		return false
	}
	return true
}

func retrieveIDByID(query string, errorText string, id string) (string, error) {
	rows := db.Query(db.GetQuery(query), id)
	if !rows.Next() {
		return "", errors.New(query)
	}

	var resultID string
	err := rows.Scan(&resultID)
	if err != nil {
		log.Fatal(err)
	}
	db.CloseRows(rows)

	return resultID, nil
}

func RetrieveOrganizationIDByRoleID(roleID string) (string, error) {
	return retrieveIDByID("get_organization_id_by_role_id", "role or organization does not exist", roleID)
}

func RetrieveOrganizationIDByReasonID(reasonID string) (string, error) {
	return retrieveIDByID("get_organization_id_by_reason_id", "reason or organization does not exist", reasonID)
}

func RetrieveOrganizationIDByCategoryID(categoryID string) (string, error) {
	return retrieveIDByID("get_organization_id_by_category_id", "category or organization does not exist", categoryID)
}

func RetrieveOrganizationIDByGroupID(groupID string) (string, error) {
	return retrieveIDByID("get_organization_id_by_group_id", "group or organization does not exist", groupID)
}

func RetrieveOrganizationIDByItemID(itemID string) (string, error) {
	return retrieveIDByID("get_organization_id_by_item_id", "item or organization does not exist", itemID)
}

func RetrieveRoleIDByRolePermissionID(rolePermissionID string) (string, error) {
	return retrieveIDByID("get_role_id_by_role_permission_id", "role permission or role does not exist", rolePermissionID)
}

func RetrieveReasonIDByReasonGradeID(reasonGradeID string) (string, error) {
	return retrieveIDByID("get_reason_id_by_reason_grade_id", "reason grade or reason does not exist", reasonGradeID)
}

func RetrieveMemberRoleIDByGroupID(groupID string) (string, error) {
	return retrieveIDByID("get_member_role_id_by_group_id", "group or role does not exist", groupID)
}

func RetrieveAdministratorRoleIDByGroupID(groupID string) (string, error) {
	return retrieveIDByID("get_admin_role_id_by_group_id", "group or role does not exist", groupID)
}

func RetrieveUserIDByRoleID(roleID string) (string, error) {
	return retrieveIDByID("get_user_id_by_role_id", "role or user does not exist", roleID)
}

func RetrievePriceGroupIDByPriceID(priceID string) (string, error) {
	return retrieveIDByID("get_price_group_id_by_price_id", "price or price group does not exist", priceID)
}

func RetrieveItemIDByPriceGroupID(priceGroupID string) (string, error) {
	return retrieveIDByID("get_item_id_by_price_group_id", "price group or item does not exist", priceGroupID)
}

func RetrieveGradeChangeGroupByID(gradeChangeGroupID string) (GradeChangeGroup, error) {
	var gradeChangeGroup GradeChangeGroup

	rows := db.Query(db.GetQuery("get_grade_change_group_by_id"), gradeChangeGroupID)
	if !rows.Next() {
		return gradeChangeGroup, errors.New("grade change group does not exist")
	}

	err := gradeChangeGroup.Scan(rows)
	if err != nil {
		log.Fatal(err)
	}
	db.CloseRows(rows)

	return gradeChangeGroup, nil
}

func RetrieveCategoryByIDandOrganizaionID(categoryID string, organizationID string) (Category, error) {
	var category Category

	rows := db.Query(db.GetQuery("get_category_by_id_and_organization_id"), categoryID, organizationID)
	if !rows.Next() {
		return category, errors.New("category does not exist in this organization")
	}

	err := rows.Scan(&category.ID, &category.Name, &category.Base, &category.Organization, &category.AllowNegative)
	if err != nil {
		log.Fatal(err)
	}
	db.CloseRows(rows)

	return category, nil
}

func RetrieveGradeByUserIDandCategoryID(userID string, categoryID string) (Grade, error) {
	var grade Grade

	rows := db.Query(db.GetQuery("get_grade_by_user_id_and_category_id"), userID, categoryID)
	if !rows.Next() {
		return grade, errors.New("grade does not exist")
	}

	err := rows.Scan(&grade.ID, &grade.Value)
	if err != nil {
		log.Fatal(err)
	}
	db.CloseRows(rows)

	return grade, nil
}
