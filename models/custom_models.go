package models

type User struct {
	ID         UUID    `json:"id"`
	Email      string  `json:"email"`
	Surname    string  `json:"surname"`
	Name       string  `json:"name"`
	Patronymic string  `json:"patronymic"`
	ImgPath    *string `json:"img_path"`
}

type Member struct {
	IsConfirmed    bool `json:"is_confirmed"`
	UserID         UUID
	OrganizationID UUID
}

type Organization struct {
	ID                   UUID    `json:"id"`
	Name                 string  `json:"name"`
	ImgPath              *string `json:"img_path"`
	CategoriesEnabled    bool    `json:"categories_enabled"`
	ReasonsEnabled       bool    `json:"reasons_enabled"`
	StatisticsEnabled    bool    `json:"statistics_enabled"`
	MarketEnabled        bool    `json:"market_enabled"`
	AchievementsEnabled  bool    `json:"achievements_enabled"`
	TeamplayEnabled      bool    `json:"teamplay_enabled"`
	AnonymizationEnabled bool    `json:"anonymization_enabled"`
}

type Role struct {
	ID           UUID   `json:"id"`
	Name         string `json:"name"`
	Organization *UUID  `json:"organization"`
	Display      bool   `json:"display"`
}

type Reason struct {
	ID           UUID   `json:"id"`
	Organization UUID   `json:"organization"`
	Text         string `json:"text"`
}

type GradeChangeGroup struct {
	ID           UUID   `json:"id"`
	Organization UUID   `json:"organization"`
	Issuer       UUID   `json:"issuer"`
	User         UUID   `json:"user"`
	Text         string `json:"text"`
}

type Group struct {
	ID           UUID    `json:"id"`
	Organization UUID    `json:"organization"`
	Name         string  `json:"name"`
	Description  *string `json:"description"`
	ImgPath      *string `json:"img_path"`
}

type Item struct {
	ID           UUID   `json:"id"`
	Organization UUID   `json:"organization"`
	Name         string `json:"name"`
	Description  string `json:"description"`
	ImgPath      string `json:"img_path"`
	IsSpecial    bool   `json:"is_special"`
	IsIndividual bool   `json:"is_individual"`
}

type PriceGroup struct {
	ID            UUID `json:"id"`
	ItemID        UUID `json:"item_id"`
	IsConjunction bool `json:"is_conjunction"`
}

type Purchase struct {
	ID                 UUID  `json:"id"`
	Buyer              UUID  `json:"buyer"`
	ItemID             UUID  `json:"item_id"`
	GradeChangeGroupID UUID  `json:"grade_change_group_id"`
}
