package models

import (
	"database/sql"
)

// USERS

func (u *User) Scan(rows *sql.Rows) error {
	return rows.Scan(&u.ID, &u.Email, &u.Surname, &u.Name, &u.Patronymic, &u.ImgPath)
}

func (u *User) GetID() UUID {
	return u.ID
}

func (u *Users) Init() {
	u.Data = make([]*User, 0)
}

func (u *Users) NewEntity() ModelEntity {
	return &User{}
}

func (u *Users) Append(entity *ModelEntity) {
	var e = (*entity).(*User)
	u.Data = append(u.Data, e)
}

func (u *Users) SetLast(last UUID) {
	u.Last = last
}

// MEMBERS

func (m *Member) Scan(rows *sql.Rows) error {
	return rows.Scan(&m.UserID, &m.OrganizationID, &m.IsConfirmed)
}

func (m *Member) GetID() UUID {
	return m.UserID
}

func (m *Members) Init() {
	m.Data = make([]*Member, 0)
}

func (m *Members) NewEntity() ModelEntity {
	return &Member{}
}

func (m *Members) Append(entity *ModelEntity) {
	var e = (*entity).(*Member)
	m.Data = append(m.Data, e)
}

func (m *Members) SetLast(last UUID) {
	m.Last = last
}

// ROLES

func (r *Role) Scan(rows *sql.Rows) error {
	return rows.Scan(&r.ID, &r.Name, &r.Organization, &r.Display)
}

func (r *Role) GetID() UUID {
	return r.ID
}

func (r *Roles) Init() {
	r.Data = make([]*Role, 0)
}

func (r *Roles) NewEntity() ModelEntity {
	return &Role{}
}

func (r *Roles) Append(entity *ModelEntity) {
	var e = (*entity).(*Role)
	r.Data = append(r.Data, e)
}

func (r *Roles) SetLast(last UUID) {
	r.Last = last
}

// ROLE PERMISSIONS

func (r *RolePermission) Scan(rows *sql.Rows) error {
	return rows.Scan(&r.ID, &r.Role, &r.Type, &r.Resource, &r.Self, &r.Create, &r.Read, &r.Update, &r.Delete)
}

func (r *RolePermission) GetID() UUID {
	return r.ID
}

func (r *RolePermissions) Init() {
	r.Data = make([]*RolePermission, 0)
}

func (r *RolePermissions) NewEntity() ModelEntity {
	return &RolePermission{}
}

func (r *RolePermissions) Append(entity *ModelEntity) {
	var e = (*entity).(*RolePermission)
	r.Data = append(r.Data, e)
}

func (r *RolePermissions) SetLast(last UUID) {
	r.Last = last
}

// REASONS

func (r *Reason) Scan(rows *sql.Rows) error {
	return rows.Scan(&r.ID, &r.Organization, &r.Text)
}

func (r *Reason) GetID() UUID {
	return r.ID
}

func (r *Reasons) Init() {
	r.Data = make([]*Reason, 0)
}

func (r *Reasons) NewEntity() ModelEntity {
	return &Reason{}
}

func (r *Reasons) Append(entity *ModelEntity) {
	var e = (*entity).(*Reason)
	r.Data = append(r.Data, e)
}

func (g *Reasons) SetLast(last UUID) {
	g.Last = last
}

// REASON GRADES

func (r *ReasonGrade) Scan(rows *sql.Rows) error {
	return rows.Scan(&r.ID, &r.Reason, &r.Category, &r.Change)
}

func (r *ReasonGrade) GetID() UUID {
	return r.ID
}

// CATEGORIES

func (c *Category) Scan(rows *sql.Rows) error {
	return rows.Scan(&c.ID, &c.Name, &c.Base, &c.Organization, &c.AllowNegative)
}

func (c *Category) GetID() UUID {
	return c.ID
}

func (c *Categories) Init() {
	c.Data = make([]*Category, 0)
}

func (c *Categories) NewEntity() ModelEntity {
	return &Category{}
}

func (c *Categories) Append(entity *ModelEntity) {
	var e = (*entity).(*Category)
	c.Data = append(c.Data, e)
}

func (c *Categories) SetLast(last UUID) {
	c.Last = last
}

// GROUPS

func (g *Group) Scan(rows *sql.Rows) error {
	return rows.Scan(&g.ID, &g.Organization, &g.Name, &g.Description, &g.ImgPath)
}

func (g *Group) GetID() UUID {
	return g.ID
}

func (g *Groups) Init() {
	g.Data = make([]*Group, 0)
}

func (g *Groups) NewEntity() ModelEntity {
	return &Group{}
}

func (g *Groups) Append(entity *ModelEntity) {
	var e = (*entity).(*Group)
	g.Data = append(g.Data, e)
}

func (g *Groups) SetLast(last UUID) {
	g.Last = last
}

// ORGANIZATIONS

func (o *Organization) Scan(rows *sql.Rows) error {
	return rows.Scan(&o.ID, &o.Name, &o.ImgPath, &o.CategoriesEnabled, &o.ReasonsEnabled, &o.StatisticsEnabled,
		&o.MarketEnabled, &o.AchievementsEnabled, &o.TeamplayEnabled, &o.AnonymizationEnabled)
}

func (o *Organization) GetID() UUID {
	return o.ID
}

func (o *Organizations) Init() {
	o.Data = make([]*Organization, 0)
}

func (o *Organizations) NewEntity() ModelEntity {
	return &Organization{}
}

func (o *Organizations) Append(entity *ModelEntity) {
	var e = (*entity).(*Organization)
	o.Data = append(o.Data, e)
}

func (o *Organizations) SetLast(last UUID) {
	o.Last = last
}

// GRADES

func (g *Grade) Scan(rows *sql.Rows) error {
	return rows.Scan(&g.ID, &g.User, &g.Category, &g.Value)
}

func (g *Grade) GetID() UUID {
	return g.ID
}

func (g *Grades) Init() {
	g.Data = make([]*Grade, 0)
}

func (g *Grades) NewEntity() ModelEntity {
	return &Grade{}
}

func (g *Grades) Append(entity *ModelEntity) {
	var e = (*entity).(*Grade)
	g.Data = append(g.Data, e)
}

func (g *Grades) SetLast(last UUID) {
	g.Last = last
}

// GRADE CHANGES

func (g *GradeChange) Scan(rows *sql.Rows) error {
	return rows.Scan(&g.ID, &g.GradeChangeGroup, &g.Category, &g.Change)
}

func (g *GradeChange) GetID() UUID {
	return g.ID
}

func (g *GradeChanges) Init() {
	g.Data = make([]*GradeChange, 0)
}

func (g *GradeChanges) NewEntity() ModelEntity {
	return &GradeChange{}
}

func (g *GradeChanges) Append(entity *ModelEntity) {
	var e = (*entity).(*GradeChange)
	g.Data = append(g.Data, e)
}

func (g *GradeChanges) SetLast(last UUID) {
	g.Last = last
}

// GRADE CHANGE GROUP

func (g *GradeChangeGroup) Scan(rows *sql.Rows) error {
	return rows.Scan(&g.ID, &g.Organization, &g.Issuer, &g.User, &g.Text)
}

func (g *GradeChangeGroup) GetID() UUID {
	return g.ID
}

func (g *GradeChangeGroups) Init() {
	g.Data = make([]*GradeChangeGroup, 0)
}

func (g *GradeChangeGroups) NewEntity() ModelEntity {
	return &GradeChangeGroup{}
}

func (g *GradeChangeGroups) Append(entity *ModelEntity) {
	var e = (*entity).(*GradeChangeGroup)
	g.Data = append(g.Data, e)
}

func (g *GradeChangeGroups) SetLast(last UUID) {
	g.Last = last
}

// ITEMS

func (i *Item) Scan(rows *sql.Rows) error {
	return rows.Scan(&i.ID, &i.Organization, &i.Name, &i.Description, &i.ImgPath, &i.IsSpecial, &i.IsIndividual)
}

func (i *Item) GetID() UUID {
	return i.ID
}

func (i *Items) Init() {
	i.Data = make([]*Item, 0)
}

func (i *Items) NewEntity() ModelEntity {
	return &Item{}
}

func (i *Items) Append(entity *ModelEntity) {
	var e = (*entity).(*Item)
	i.Data = append(i.Data, e)
}

func (i *Items) SetLast(last UUID) {
	i.Last = last
}

// PRICE GROUPS

func (p *PriceGroup) Scan(rows *sql.Rows) error {
	return rows.Scan(&p.ID, &p.ItemID, &p.IsConjunction)
}

func (p *PriceGroup) GetID() UUID {
	return p.ID
}

func (p *PriceGroups) Init() {
	p.Data = make([]*PriceGroup, 0)
}

func (p *PriceGroups) NewEntity() ModelEntity {
	return &PriceGroup{}
}

func (p *PriceGroups) Append(entity *ModelEntity) {
	var e = (*entity).(*PriceGroup)
	p.Data = append(p.Data, e)
}

func (p *PriceGroups) SetLast(last UUID) {
	p.Last = last
}

// PRICES

func (p *Price) Scan(rows *sql.Rows) error {
	return rows.Scan(&p.ID, &p.PriceGroupID, &p.CategoryID, &p.Price)
}

func (p *Price) GetID() UUID {
	return p.ID
}

func (p *Prices) Init() {
	p.Data = make([]*Price, 0)
}

func (p *Prices) NewEntity() ModelEntity {
	return &Price{}
}

func (p *Prices) Append(entity *ModelEntity) {
	var e = (*entity).(*Price)
	p.Data = append(p.Data, e)
}

func (p *Prices) SetLast(last UUID) {
	p.Last = last
}

// PURCHASES

func (p *Purchase) Scan(rows *sql.Rows) error {
	return rows.Scan(&p.ID, &p.Buyer, &p.ItemID, &p.GradeChangeGroupID)
}

func (p *Purchase) GetID() UUID {
	return p.ID
}

func (p *Purchases) Init() {
	p.Data = make([]*Purchase, 0)
}

func (p *Purchases) NewEntity() ModelEntity {
	return &Purchase{}
}

func (p *Purchases) Append(entity *ModelEntity) {
	var e = (*entity).(*Purchase)
	p.Data = append(p.Data, e)
}

func (p *Purchases) SetLast(last UUID) {
	p.Last = last
}
