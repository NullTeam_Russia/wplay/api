package models

import (
	"fmt"
	"io"

	"github.com/google/uuid"
)

type UUID string

func (u UUID) MarshalGQL(w io.Writer) {
	fmt.Fprintf(w, `"%s"`, string(u))
}

func (u *UUID) UnmarshalGQL(v interface{}) error {
	switch v := v.(type) {
	case string:
		s := fmt.Sprintf("%v", v)
		_, err := uuid.Parse(s)
		if err != nil {
			return fmt.Errorf("UUID has wrong format")
		}
		*u = UUID(s)
		return nil
	default:
		return fmt.Errorf("UUID has wrong format")
	}
}

func (u UUID) String() *string {
	s := string(u)
	return &s
}
