package conf

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

const ConfFile = "conf/conf.json"

type Conf struct {
	DB     DB
	Mail   Mail
	Server Server
}

type DB struct {
	Host     string
	Port     int
	User     string
	Password string
	DBname   string
	Version  uint
}

type Mail struct {
	Username         string
	Password         string
	Host             string
	Port             int
	ConfirmationLink string
}

type Server struct {
	Stage       string
	TLSCertPath string
	PrivKeyPath string
}

var Configuration Conf

const StageLocal string = "local"
const StageDev string = "development"
const StageProd string = "production"

func Init() {
	jsonFile, err := os.Open(ConfFile)
	if err != nil {
		fmt.Println(err)
	}

	byteValue, _ := ioutil.ReadAll(jsonFile)
	err = json.Unmarshal(byteValue, &Configuration)
	if err != nil {
		fmt.Println(err)
	}

	defer jsonFile.Close()
}

func Update() {
	file, _ := json.MarshalIndent(Configuration, "", " ")
	err := ioutil.WriteFile(ConfFile, file, 0644)
	if err != nil {
		log.Fatal(err)
	}
}
