package db

import (
	"fmt"
	"log"
	"strings"
)

func BuildRequest(requestType string, tableName string, fields []string, values []string) string {
	if len(fields) != len(values) {
		log.Fatal("number of fields and values does not match")
	}

	if len(fields) == 1 {
		return fmt.Sprintf("%s %s SET %s=%s", requestType, tableName, fields[0], values[0])
	}

	var fieldsBuilder, valuesBuilder strings.Builder
	for _, field := range fields {
		fieldsBuilder.WriteString(field)
		fieldsBuilder.WriteString(",")
	}
	for _, value := range values {
		valuesBuilder.WriteString(value)
		valuesBuilder.WriteString(",")
	}
	fieldsResult := fieldsBuilder.String()[:fieldsBuilder.Len()-1]
	valuesResult := valuesBuilder.String()[:valuesBuilder.Len()-1]
	return fmt.Sprintf("%s %s SET (%s)=(%s)", requestType, tableName, fieldsResult, valuesResult)
}
