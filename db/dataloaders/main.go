package dataloaders

import (
	"context"
	"net/http"
)

const loadersKey = "dataloaders"

type Loaders struct {
	ReasonGradeByReasonID           ReasonGradeByUUID
	GradeChangeByGradeChangeGroupID GradeChangeByUUID
}

func DataloadersMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := context.WithValue(r.Context(), loadersKey, &Loaders{
			ReasonGradeByReasonID:           reasonGradeByReasonID,
			GradeChangeByGradeChangeGroupID: gradeChangeByGradeChangeGroupID,
		})
		r = r.WithContext(ctx)
		next.ServeHTTP(w, r)
	})
}

func GetDataloadersFromContext(ctx context.Context) *Loaders {
	return ctx.Value(loadersKey).(*Loaders)
}
