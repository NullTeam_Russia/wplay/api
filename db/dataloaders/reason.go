package dataloaders

import (
	"fmt"
	"strings"
	"time"

	"nullteam.info/wplay/api/db"
	"nullteam.info/wplay/api/models"
)

//go:generate go run github.com/vektah/dataloaden ReasonGradeByUUID nullteam.info/wplay/api/models.UUID  []*nullteam.info/wplay/api/models.ReasonGrade

var reasonGradeByReasonID = ReasonGradeByUUID{
	maxBatch: 100,
	wait:     1 * time.Millisecond,
	fetch: func(ids []models.UUID) ([][]*models.ReasonGrade, []error) {
		placeholders := make([]string, len(ids))
		args := make([]interface{}, len(ids))
		for i := 0; i < len(ids); i++ {
			placeholders[i] = fmt.Sprintf("$%d", i+1)
			args[i] = ids[i]
		}

		rows := db.Query("SELECT uuid, reason_id, category_id, change FROM reason_grades WHERE reason_id IN ("+strings.Join(placeholders, ",")+")",
			args...,
		)
		defer db.CloseRows(rows)

		var reasonGrades []*models.ReasonGrade
		for rows.Next() {
			var reasonGrade models.ReasonGrade
			reasonGrade.Scan(rows)
			reasonGrades = append(reasonGrades, &reasonGrade)
		}

		result := make([][]*models.ReasonGrade, len(ids))
		for i, id := range ids {
			for j := range reasonGrades {
				if reasonGrades[j].Reason == id {
					result[i] = append(result[i], reasonGrades[j])
				}
			}
			i++
		}

		return result, nil
	},
}
