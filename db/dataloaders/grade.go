package dataloaders

import (
	"fmt"
	"strings"
	"time"

	"nullteam.info/wplay/api/db"
	"nullteam.info/wplay/api/models"
)

//go:generate go run github.com/vektah/dataloaden GradeChangeByUUID nullteam.info/wplay/api/models.UUID  []*nullteam.info/wplay/api/models.GradeChange

var gradeChangeByGradeChangeGroupID = GradeChangeByUUID{
	maxBatch: 100,
	wait:     1 * time.Millisecond,
	fetch: func(ids []models.UUID) ([][]*models.GradeChange, []error) {
		placeholders := make([]string, len(ids))
		args := make([]interface{}, len(ids))
		for i := 0; i < len(ids); i++ {
			placeholders[i] = fmt.Sprintf("$%d", i+1)
			args[i] = ids[i]
		}

		rows := db.Query("SELECT uuid, grade_change_group, category_id, change FROM grade_changes WHERE grade_change_group IN ("+strings.Join(placeholders, ",")+")",
			args...,
		)
		defer db.CloseRows(rows)

		var gradeChanges []*models.GradeChange
		for rows.Next() {
			var gradeChange models.GradeChange
			gradeChange.Scan(rows)
			gradeChanges = append(gradeChanges, &gradeChange)
		}

		result := make([][]*models.GradeChange, len(ids))
		for i, id := range ids {
			for j := range gradeChanges {
				if gradeChanges[j].GradeChangeGroup == id {
					result[i] = append(result[i], gradeChanges[j])
				}
			}
			i++
		}

		return result, nil
	},
}
