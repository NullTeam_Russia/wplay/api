INSERT INTO role_permissions (uuid,
                              created_at,
                              role_id,
                              permission_type,
                              resource,
                              self,
                              permission_to_create,
                              permission_to_read,
                              permission_to_update,
                              permission_to_delete)
VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10);
