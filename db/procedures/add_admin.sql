INSERT INTO users (uuid, email, password, surname, name, patronymic, verified)
VALUES ('3ce7e184-de94-4507-bdbd-9e6dd11080da', 'admin',
        'bd4b39b37c13a3eca646d9bac11e7d3a3d167e0e3b8f7a88a467ab55032ea52bbe626844bfbb2e9cecb9e00483dd6d9216042ef19e620f121fe6d6fc6e17889f',
        'Admin', 'Admin', 'Admin', true)
ON CONFLICT DO NOTHING;

INSERT INTO roles (uuid, name, display)
VALUES ('9c6fb2b4-e493-4538-b52a-a4b2bee205b6', 'Admin', true)
ON CONFLICT DO NOTHING;

INSERT INTO role_permissions (uuid, role_id, permission_type, self, permission_to_create, permission_to_read,
                              permission_to_update, permission_to_delete)
VALUES ('48fcc988-1410-4cfd-86be-fa9c108dcc68', '9c6fb2b4-e493-4538-b52a-a4b2bee205b6', 1, false, true, true, true, true)
ON CONFLICT DO NOTHING;

INSERT INTO user_roles (uuid, user_id, role_id, display)
VALUES ('499373a1-9cfa-4d3d-b785-88bf55a51800',
        '3ce7e184-de94-4507-bdbd-9e6dd11080da',
        '9c6fb2b4-e493-4538-b52a-a4b2bee205b6',
        true)
ON CONFLICT DO NOTHING;