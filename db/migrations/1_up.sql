create table "users" (
  "uuid" uuid primary key,
  "created_at" timestamp,
  "deleted_at" timestamp,
  "email" varchar,
  "password" varchar,
  "surname" varchar,
  "name" varchar,
  "patronymic" varchar,
  "img_path" varchar,
  "verified" boolean default false,
  "verification_link" varchar,
  "verification_email_send_date" timestamp
);

create table "roles" (
  "uuid" uuid primary key,
  "created_at" timestamp,
  "deleted_at" timestamp,
  "name" varchar,
  "organization_id" uuid,
  "technical" boolean default false,
  "display" boolean
);

create table "role_permissions" (
  "uuid" uuid primary key,
  "created_at" timestamp,
  "deleted_at" timestamp,
  "role_id" uuid,
  "permission_type" integer,
  "resource" uuid,
  "self" boolean,
  "permission_to_create" boolean,
  "permission_to_read" boolean,
  "permission_to_update" boolean,
  "permission_to_delete" boolean
);

create table "user_roles" (
  "uuid" uuid primary key,
  "created_at" timestamp,
  "deleted_at" timestamp,
  "user_id" uuid,
  "role_id" uuid,
  "display" boolean
);

create table "organizations" (
  "uuid" uuid primary key,
  "created_at" timestamp,
  "deleted_at" timestamp,
  "name" varchar,
  "img_path" varchar,
  "categories" boolean,
  "reasons" boolean,
  "statistics" boolean,
  "market" boolean,
  "achievements" boolean,
  "teamplay" boolean,
  "anonymization" boolean
);

create table "categories" (
  "uuid" uuid primary key,
  "created_at" timestamp,
  "deleted_at" timestamp,
  "organization_id" uuid,
  "name" varchar,
  "base" boolean default false,
  "allow_negative" boolean
);

create table "grades" (
  "uuid" uuid primary key,
  "created_at" timestamp,
  "deleted_at" timestamp,
  "user_id" uuid,
  "category_id" uuid,
  "value" integer
);

create table "achievements" (
  "uuid" uuid primary key,
  "created_at" timestamp,
  "deleted_at" timestamp,
  "organization_id" uuid,
  "name" varchar,
  "description" varchar,
  "img_path" varchar,
  "is_special" boolean,
  "is_individal" boolean,
  "conditions" jsonb
);

create table "granted_achievements" (
  "uuid" uuid primary key,
  "created_at" timestamp,
  "deleted_at" timestamp,
  "granted_to" uuid,
  "achievement_id" uuid
);

create table "items" (
  "uuid" uuid primary key,
  "created_at" timestamp,
  "deleted_at" timestamp,
  "organization_id" uuid,
  "name" varchar,
  "description" varchar,
  "img_path" varchar,
  "is_special" boolean,
  "is_individual" boolean
);

create table "price_groups" (
    "uuid" uuid primary key,
    "created_at" timestamp,
    "deleted_at" timestamp,
    "item_id" uuid,
    "is_conjunction" boolean default false
);

create table "prices" (
    "uuid" uuid primary key,
    "created_at" timestamp,
    "deleted_at" timestamp,
    "price_group_id" uuid,
    "category_id" uuid,
    "price" integer
);

create table "purchases" (
  "uuid" uuid primary key,
  "created_at" timestamp,
  "deleted_at" timestamp,
  "buyer" uuid,
  "item_id" uuid,
  "grade_change_group_id" uuid
);

create table "reasons" (
  "uuid" uuid primary key,
  "created_at" timestamp,
  "deleted_at" timestamp,
  "organization_id" uuid,
  "text" varchar
);

create table "reason_grades" (
  "uuid" uuid primary key,
  "created_at" timestamp,
  "deleted_at" timestamp,
  "reason_id" uuid,
  "category_id" uuid,
  "change" integer
);

create table "grade_change_groups" (
  "uuid" uuid primary key,
  "created_at" timestamp,
  "deleted_at" timestamp,
  "issuer_id" uuid,
  "user_id" uuid,
  "organization_id" uuid,
  "text" varchar
);

create table "grade_changes" (
  "uuid" uuid primary key,
  "grade_change_group" uuid,
  "category_id" uuid,
  "change" integer
);

create table "groups" (
  "uuid" uuid primary key,
  "created_at" timestamp,
  "deleted_at" timestamp,
  "organization_id" uuid,
  "name" varchar,
  "description" varchar,
  "img_path" varchar
);

create table "group_invites" (
  "uuid" uuid primary key,
  "created_at" timestamp,
  "deleted_at" timestamp,
  "user_id" uuid,
  "group_id" uuid,
  "confirmed" boolean default false
);

create table "group_fundrisings" (
  "uuid" uuid primary key,
  "created_at" timestamp,
  "deleted_at" timestamp,
  "group_id" uuid,
  "item_id" uuid,
  "is_completed" boolean
);

create table "group_funds" (
  "uuid" uuid primary key,
  "created_at" timestamp,
  "deleted_at" timestamp,
  "group_fundrising_id" uuid,
  "grade_id" uuid,
  "change" integer
);

create table "periods" (
  "uuid" uuid primary key,
  "created_at" timestamp,
  "deleted_at" timestamp,
  "organization_id" uuid,
  "start_date" timestamp,
  "end_date" timestamp,
  "name" varchar,
  "TBF" varchar
);

create table "billing_accounts" (
  "uuid" uuid primary key,
  "created_at" timestamp,
  "deleted_at" timestamp,
  "organization_id" uuid,
  "TBF" varchar
);

create table "organization_invites" (
  "uuid" uuid primary key,
  "created_at" timestamp,
  "deleted_at" timestamp,
  "user_id" uuid,
  "organization_id" uuid,
  "confirmed" boolean default false
);

create table "sessions" (
  "uuid" uuid primary key,
  "created_at" timestamp,
  "deleted_at" timestamp,
  "user_id" uuid,
  "ip" varchar,
  "access_token" varchar unique,
  "access_token_expiry_date" timestamp
);

alter TABLE "roles" ADD FOREIGN KEY ("organization_id") REFERENCES "organizations" ("uuid") ON delete CASCADE;

alter TABLE "role_permissions" ADD FOREIGN KEY ("role_id") REFERENCES "roles" ("uuid") ON delete CASCADE;

alter TABLE "user_roles" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("uuid") ON delete CASCADE;
alter TABLE "user_roles" ADD FOREIGN KEY ("role_id") REFERENCES "roles" ("uuid") ON delete CASCADE;

alter TABLE "categories" ADD FOREIGN KEY ("organization_id") REFERENCES "organizations" ("uuid") ON delete CASCADE;

alter TABLE "grades" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("uuid") ON delete CASCADE;
alter TABLE "grades" ADD FOREIGN KEY ("category_id") REFERENCES "categories" ("uuid") ON delete CASCADE;

alter TABLE "achievements" ADD FOREIGN KEY ("organization_id") REFERENCES "organizations" ("uuid") ON delete CASCADE;

alter TABLE "granted_achievements" ADD FOREIGN KEY ("achievement_id") REFERENCES "achievements" ("uuid") ON delete CASCADE;

alter TABLE "items" ADD FOREIGN KEY ("organization_id") REFERENCES "organizations" ("uuid") ON delete CASCADE;

alter TABLE "price_groups" ADD FOREIGN KEY ("item_id") REFERENCES "items" ("uuid") ON delete CASCADE;

alter TABLE "prices" ADD FOREIGN KEY ("price_group_id") REFERENCES "price_groups" ("uuid") ON delete CASCADE;
alter TABLE "prices" ADD FOREIGN KEY ("category_id") REFERENCES "categories" ("uuid") ON delete CASCADE;

alter TABLE "purchases" ADD FOREIGN KEY ("item_id") REFERENCES "items" ("uuid") ON delete CASCADE;
alter TABLE "purchases" ADD FOREIGN KEY ("grade_change_group_id") REFERENCES "grade_change_groups" ("uuid") ON delete CASCADE;

alter TABLE "reasons" ADD FOREIGN KEY ("organization_id") REFERENCES "organizations" ("uuid") ON delete CASCADE;

alter TABLE "reason_grades" ADD FOREIGN KEY ("reason_id") REFERENCES "reasons" ("uuid") ON delete CASCADE;
alter TABLE "reason_grades" ADD FOREIGN KEY ("category_id") REFERENCES "categories" ("uuid") ON delete CASCADE;

alter TABLE "grade_change_groups" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("uuid") ON delete CASCADE;
alter TABLE "grade_change_groups" ADD FOREIGN KEY ("issuer_id") REFERENCES "users" ("uuid") ON delete CASCADE;

alter TABLE "grade_changes" ADD FOREIGN KEY ("category_id") REFERENCES "categories" ("uuid") ON delete CASCADE;
alter TABLE "grade_changes" ADD FOREIGN KEY ("grade_change_group") REFERENCES "grade_change_groups" ("uuid") ON delete CASCADE;

alter TABLE "groups" ADD FOREIGN KEY ("organization_id") REFERENCES "organizations" ("uuid") ON delete CASCADE;

alter TABLE "group_invites" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("uuid") ON delete CASCADE;
alter TABLE "group_invites" ADD FOREIGN KEY ("group_id") REFERENCES "groups" ("uuid") ON delete CASCADE;

alter TABLE "group_fundrisings" ADD FOREIGN KEY ("group_id") REFERENCES "groups" ("uuid") ON delete CASCADE;
alter TABLE "group_fundrisings" ADD FOREIGN KEY ("item_id") REFERENCES "items" ("uuid") ON delete CASCADE;

alter TABLE "group_funds" ADD FOREIGN KEY ("group_fundrising_id") REFERENCES "group_fundrisings" ("uuid") ON delete CASCADE;
alter TABLE "group_funds" ADD FOREIGN KEY ("grade_id") REFERENCES "grades" ("uuid") ON delete CASCADE;

alter TABLE "periods" ADD FOREIGN KEY ("organization_id") REFERENCES "organizations" ("uuid") ON delete CASCADE;

alter TABLE "billing_accounts" ADD FOREIGN KEY ("organization_id") REFERENCES "organizations" ("uuid") ON delete CASCADE;

alter TABLE "organization_invites" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("uuid") ON delete CASCADE;
alter TABLE "organization_invites" ADD FOREIGN KEY ("organization_id") REFERENCES "organizations" ("uuid") ON delete CASCADE;

alter TABLE "sessions" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("uuid") ON delete CASCADE;
