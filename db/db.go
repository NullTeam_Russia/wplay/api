package db

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"

	_ "github.com/lib/pq"
	"nullteam.info/wplay/api/conf"
)

var Database *sql.DB
var dbConfig *conf.DB
var currentTransaction *sql.Tx

func InitPostgres(db *conf.DB) {
	dbConfig = db

	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", db.Host, db.Port, db.User, db.Password, db.DBname)

	var err error
	Database, err = sql.Open("postgres", psqlInfo)

	if err != nil {
		log.Fatal(err)
	}

	err = Database.Ping()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Successfully connected to Database.")
}

func MigrationUpMax() {
	files, err := ioutil.ReadDir("db/migrations")
	if err != nil {
		log.Fatal(err)
	}

	maxVersion := 0

	for _, f := range files {
		value, _ := strconv.Atoi(strings.Split(f.Name(), "_")[0])
		if value > maxVersion {
			maxVersion = value
		}
	}

	Version(uint(maxVersion))
}

func MigrationUp() {
	if !isMigrationFileExist(dbConfig.Version+1, true) {
		fmt.Println("No version to upgrade.")
		return
	}

	dbConfig.Version++
	executeMigrationFile(dbConfig.Version, true)
	conf.Update()

	fmt.Println("Migrated to version ", dbConfig.Version)
}

func MigrationDown() {
	if !isMigrationFileExist(dbConfig.Version, false) {
		fmt.Println("No version to downgrade.")
		return
	}

	executeMigrationFile(dbConfig.Version, false)
	dbConfig.Version--
	conf.Update()

	fmt.Println("Migrated to version ", dbConfig.Version)
}

func Version(version uint) {
	if version < 0 {
		fmt.Println("Cannot migrate to version lower than zero")
		return
	}

	for version != dbConfig.Version {
		if dbConfig.Version < version {
			MigrationUp()
		} else {
			MigrationDown()
		}
	}
}

func Shutdown() {
	err := Database.Close()

	if err != nil {
		log.Fatal(err)
	}

	dbConfig = nil
	Database = nil
}

func executeMigrationFile(version uint, isUp bool) {
	request := ReadFile(getMigrationFileName(version, isUp))
	_, err := Database.Exec(request)
	if err != nil {
		log.Fatal(err)
	}
}

func getMigrationFileName(version uint, isUp bool) string {
	if isUp {
		return fmt.Sprintf("db/migrations/%d_%s.sql", version, "up")
	} else {
		return fmt.Sprintf("db/migrations/%d_%s.sql", version, "down")
	}
}

func isMigrationFileExist(version uint, isUp bool) bool {
	if _, err := os.Stat(getMigrationFileName(version, isUp)); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}

func BeginTransaction() {
	transaction, err := Database.Begin()
	if err != nil {
		log.Fatal("Error: " + err.Error())
	}
	currentTransaction = transaction
}

func RollbackTransaction() {
	if currentTransaction != nil {
		err := currentTransaction.Rollback()
		if err != nil {
			log.Fatal("Error: " + err.Error())
		}
		currentTransaction = nil
	}
}

func EndTransaction() {
	if currentTransaction != nil {
		err := currentTransaction.Commit()
		if err != nil {
			log.Fatal("Error: " + err.Error())
		}
		currentTransaction = nil
	}
}

func Query(query string, args ...interface{}) *sql.Rows {
	var rows *sql.Rows
	var err error

	if currentTransaction == nil {
		rows, err = Database.Query(query, args...)
	} else {
		rows, err = currentTransaction.Query(query, args...)
	}

	if err != nil {
		log.Fatal("Query: " + query + " Error: " + err.Error())
	}
	return rows
}

func Exec(query string, args ...interface{}) sql.Result {
	var result sql.Result
	var err error

	if currentTransaction == nil {
		result, err = Database.Exec(query, args...)
	} else {
		result, err = currentTransaction.Exec(query, args...)
	}

	if err != nil {
		log.Fatal("Procedure: " + query + " Error: " + err.Error())
	}
	return result
}

func RowsAffected(result *sql.Result) int64 {
	rowsAffected, err := (*result).RowsAffected()
	if err != nil {
		log.Fatal(err)
	}
	return rowsAffected
}

func CloseRows(rows *sql.Rows) {
	err := rows.Close()
	if err != nil {
		log.Fatal(err)
	}
}

func Scan(rows *sql.Rows, dest ...interface{}) {
	err := rows.Scan(dest...)
	if err != nil {
		log.Fatal(err)
	}
}
