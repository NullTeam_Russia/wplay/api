package db

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"
)

var queries map[string]string
var procedures map[string]string

func InitReader() {
	queries = make(map[string]string)
	procedures = make(map[string]string)
	readToDict("db/queries", &queries)
	readToDict("db/procedures", &procedures)
}

func ReadFile(path string) string {
	file, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal(err)
	}
	return string(file)
}

func GetQuery(name string) string {
	result := queries[name]
	if len(result) == 0 {
		log.Fatal(name)
	}
	return queries[name]
}

func GetProcedure(name string) string {
	return procedures[name]
}

func readToDict(path string, dict *map[string]string) {
	queryFiles, err := ioutil.ReadDir(path)
	if err != nil {
		log.Fatal(err)
	}

	for _, f := range queryFiles {
		fileName := strings.Split(f.Name(), ".")[0]
		(*dict)[fileName] = strings.TrimSpace(strings.ToLower(ReadFile(fmt.Sprint(path, "/", f.Name()))))
	}
}
