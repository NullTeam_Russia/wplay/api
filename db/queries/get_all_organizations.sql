select uuid,
       name,
       img_path,
       reasons,
       categories,
       statistics,
       market,
       achievements,
       teamplay,
       anonymization
from organizations;
