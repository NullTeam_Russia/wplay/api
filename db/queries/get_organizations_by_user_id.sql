select uuid,
       name,
       img_path,
       reasons,
       categories,
       statistics,
       market,
       achievements,
       teamplay,
       anonymization
from organizations where uuid in (select organization_id from organization_invites) where user_id = $1 and confirmed = true;
