select uuid from roles where uuid in (select role_id from role_permissions where
resource = $1 and
technical = true and
permission_to_create = true and
permission_to_read = true and
permission_to_update = true and
permission_to_delete = true);