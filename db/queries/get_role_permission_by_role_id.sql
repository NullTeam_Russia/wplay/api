select uuid,
       role_id,
       permission_type,
       resource,
       self,
       permission_to_create,
       permission_to_read,
       permission_to_update,
       permission_to_delete
from role_permissions
where role_id = $1;
