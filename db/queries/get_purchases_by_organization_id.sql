select uuid,
       buyer,
       item_id,
       grade_change_group_id
from purchases where item_id in (select uuid from items where organization_id = $1);