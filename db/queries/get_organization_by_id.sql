select uuid,
       name,
       img_path,
       categories,
       reasons,
       statistics,
       market,
       achievements,
       teamplay,
       anonymization
from organizations
where uuid = $1;
