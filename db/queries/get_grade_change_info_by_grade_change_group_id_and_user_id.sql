SELECT g.uuid, g.value, gc.change, c.allow_negative
FROM grade_changes gc JOIN categories c ON gc.category_id = c.uuid JOIN grades g ON gc.category_id = g.category_id
WHERE grade_change_group=$1 and user_id=$2;
