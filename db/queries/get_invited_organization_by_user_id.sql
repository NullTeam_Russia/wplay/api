select uuid,
       name,
       img_path,
       categories,
       statistics,
       reasons,
       market,
       achievements,
       teamplay,
       anonymization
from organizations where uuid in (select organization_id from organization_invites where user_id = $1 and confirmed = false);
