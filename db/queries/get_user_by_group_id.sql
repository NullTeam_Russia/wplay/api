select u.uuid, email, surname, name, patronymic, img_path
from (
  users u join group_invites gi on u.uuid = gi.user_id
) where group_id = $1 and confirmed = true;