select r.uuid, name, organization_id, r.display
from (roles r join user_roles ur on r.uuid = ur.role_id)
where user_id = $1
  and organization_id = $2;
