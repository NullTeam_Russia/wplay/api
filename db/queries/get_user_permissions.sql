select organization_id,
       permission_type,
       resource,
       permission_to_create,
       permission_to_read,
       permission_to_update,
       permission_to_delete
from ("user_roles" ur join
     (select role_id,
             organization_id,
             permission_type,
             resource,
             permission_to_create,
             permission_to_read,
             permission_to_update,
             permission_to_delete
      from ("role_permissions" rp join "roles" r on rp.role_id = r.uuid)) r
     on ur.role_id = r.role_id)
where user_id = $1;
