package mail

import (
	"fmt"
	"log"
	"net/http"
	"net/smtp"

	"github.com/matcornic/hermes/v2"
	"nullteam.info/wplay/api/conf"
	"nullteam.info/wplay/api/db"
)

var h hermes.Hermes
var auth smtp.Auth
var mailConf *conf.Mail

func InitMail(mc *conf.Mail) {
	mailConf = mc

	h = hermes.Hermes{
		Product: hermes.Product{
			Name: "WPlay",
			Link: "https://wplay.nullteam.com/",
			Logo: "http://www.duchess-france.org/wp-content/uploads/2016/01/gopher.png",
		},
	}

	auth = smtp.PlainAuth(
		"WPlay",
		mailConf.Username,
		mailConf.Password,
		mailConf.Host,
	)

	http.HandleFunc("/confirm", confirm)

	fmt.Println("Successfully initialized mail service.")
}

func confirm(w http.ResponseWriter, r *http.Request) {
	codes, ok := r.URL.Query()["code"]

	if !ok || len(codes[0]) < 1 {
		return
	}

	code := codes[0]

	_ = db.Exec("UPDATE users SET verified = true WHERE verification_link = $1;", code)

	http.ServeFile(w, r, "html/confirmed.html")
}

func generateConfirmationEmail(name string, link string) hermes.Email {
	return hermes.Email{
		Body: hermes.Body{
			Name: name,
			Intros: []string{
				"Welcome to WPlay! We're very excited to have you on board.",
			},
			Actions: []hermes.Action{
				{
					Instructions: "To get started with WPlay, please click here:",
					Button: hermes.Button{
						Color: "#22BC66", // Optional action button color
						Text:  "Confirm your account",
						Link:  mailConf.ConfirmationLink + link,
					},
				},
			},
			Outros: []string{
				"Need help, or have questions? Just reply to this email, we'd love to help.",
			},
		},
	}
}

func SendConfirmationEmail(to string, name string, link string) {
	email := generateConfirmationEmail(name, link)

	emailBody, err := h.GenerateHTML(email)
	if err != nil {
		panic(err)
	}

	addr := fmt.Sprint(mailConf.Host, ":", mailConf.Port)
	mime := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"
	subject := "Subject: Welcome to WPlay!\n"
	msg := []byte(subject + mime + "\n" + emailBody)

	err = smtp.SendMail(addr, auth, mailConf.Username, []string{to}, msg)
	if err != nil {
		log.Fatal(err)
	}
}
