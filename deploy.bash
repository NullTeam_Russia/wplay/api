#!/bin/bash
while getopts u:r:p: option
do
case "${option}"
in
u) USER=${OPTARG};;
p) PASSWORD=${OPTARG};;
r) REGISTRY=${OPTARG};;
esac
done
docker login -u $USER -p $PASSWORD $REGISTRY
if docker ps | grep -q api_1
then
docker kill -s HUP source_api_1
fi
cd /usr/app/source
docker-compose pull
docker-compose up -d
docker container prune -f
docker image prune -f